package com.exception.common;

/**
 * 基础状态接口
 *
 * @author wyj
 * @date 2022/8/18 17:47
 */
public interface StatusCode {

    String getCode();

    String getMsg();
}
