package com.exception.enumerate;

import com.exception.common.StatusCode;
import lombok.Getter;

/**
 * 响应类型枚举类(响应结果中msg的类型)
 *
 * @author wyj
 * @date 2022/8/18 17:49
 */
@Getter
public enum ResultTypeEnum implements StatusCode {
    SUCCESS("1000", "请求成功"),


    // TODO WYJ 异常类型,可自行扩充
    FAILED("2000", "请求失败"),
    PARAM_VALIDATE_ERROR("2001", "参数异常：参数校验失败"),
    APP_ERROR("2002", "业务异常：逻辑校验失败"),
    SYS_ERROR("2003", "系统错误")
    ;

    private String code;
    private String msg;

    ResultTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}