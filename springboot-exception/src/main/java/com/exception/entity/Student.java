package com.exception.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    @NotNull
    private Long id;

    @NotBlank(message = "名称不允许为空")
    private String name;

    @Max(value = 50,message = "年龄最大值不能超过50")
    private Integer age;

    @NotBlank(message = "地址不允许为空")
    private String address;

}
