package com.exception.exception;

import com.exception.common.StatusCode;
import com.exception.enumerate.ResultTypeEnum;
import lombok.Getter;

/**
 * 通用异常类：用于定义项目中所有的异常
 *
 * @author wyj
 * @date 2022/8/19 9:34
 */
@Getter
public class BusinessException extends RuntimeException {
    private String code;
    private String msg;

    // 手动设置异常
    public BusinessException(StatusCode statusCode, String message) {
        // message用于用户设置抛出错误详情，例如：当前价格-5，小于0
        super(message);
        // 状态码
        this.code = statusCode.getCode();
        // 状态码配套的msg
        this.msg = statusCode.getMsg();
    }

    // 默认异常使用APP_ERROR状态码
    public BusinessException(String message) {
        super(message);
        this.code = ResultTypeEnum.APP_ERROR.getCode();
        this.msg = ResultTypeEnum.APP_ERROR.getMsg();
    }

    public static BusinessException wrap(String msg) {
        return new BusinessException(ResultTypeEnum.APP_ERROR, msg);
    }
}