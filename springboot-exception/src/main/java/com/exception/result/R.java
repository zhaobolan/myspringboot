package com.exception.result;

import com.exception.common.StatusCode;
import com.exception.enumerate.ResultTypeEnum;
import lombok.Data;

/**
 * 控制前响应前端实体类
 *
 * @author wyj
 * @date 2022/8/18 17:45
 */
@Data
public class R {
    // 状态码
    private String code;

    // 状态信息
    private String msg;

    // 返回对象
    private Object data;

    // 手动设置返回vo
    public R(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    // 默认返回成功状态码，数据对象
    public R(Object data) {
        this.code = ResultTypeEnum.SUCCESS.getCode();
        this.msg = ResultTypeEnum.SUCCESS.getMsg();
        this.data = data;
    }

    // 返回指定状态码，数据对象
    public R(StatusCode statusCode, Object data) {
        this.code = statusCode.getCode();
        this.msg = statusCode.getMsg();
        this.data = data;
    }

    // 只返回状态码
    public R(StatusCode statusCode) {
        this.code = statusCode.getCode();
        this.msg = statusCode.getMsg();
        this.data = null;
    }
}