package com.exception.controller;

import com.exception.advice.NotControllerResponseAdvice;
import com.exception.entity.Student;
import com.exception.exception.BusinessException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/student/info")
public class StudentController {

    private static List<Student> studentList = new ArrayList<>();

    static {
        for (int i = 0; i < 10; i++) {
            studentList.add(Student.builder().id(Long.valueOf(i)).name("weiyiji" + i).address("sichuan" + i).build());
        }
    }

    @GetMapping("/getStudentById")
    public Student getStudentById(@RequestParam(value = "id") Long id) {
        if (id == 1L) {
            throw BusinessException.wrap("id为1的数据不可查询");
        }
        Student student = studentList.stream().filter(i -> i.getId().equals(id)).findFirst().get();
        return student;
    }

    @PostMapping("/saveStudent")
    @NotControllerResponseAdvice
    public String saveStudent(@Validated @RequestBody Student student) {
        studentList.add(student);
        return "success";
    }
}
