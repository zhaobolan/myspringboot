//package com.amqp.hanlder;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
///**
// * 用于监听某个队列或多个队列数据
// */
//@Component
//@RabbitListener(queues = {"wfx-fanout-quence"})
//public class ReceiveDirectHanlder2 {
//
//    @RabbitHandler//标记当前方法是用来处理消息的
//    public void recevieMsg(String message) {
//        System.out.println("订阅交换机的wfx-fanout-quence队列收到消息: 字符串=>" + message);
//    }
//}