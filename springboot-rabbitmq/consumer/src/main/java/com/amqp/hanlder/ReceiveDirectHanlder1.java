package com.amqp.hanlder;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 用于监听某个队列或多个队列数据
 */
@Component
//@RabbitListener(queuesToDeclare = { @Queue("wfx-quence") })
@RabbitListener(queues = {"normal-queue"})
public class ReceiveDirectHanlder1 {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RabbitHandler//标记当前方法是用来处理消息的
    public void recevieMsg(String msg, Channel channel, Message message) throws IOException {
        // 获取MessageId
        String messageId = message.getMessageProperties().getHeader("spring_returned_message_correlation");
        System.out.println("消息ID：" + messageId);

        // 设置key到Redis
        if (redisTemplate.opsForValue().setIfAbsent(messageId, "0", 30, TimeUnit.SECONDS)) {
            // 消费消息(模拟20秒)
            try {
                System.out.println("wfx-quence队列收到消息: =>" + msg);
                System.out.println("处理消息大约消耗20秒");
//                try {
//                    TimeUnit.SECONDS.sleep(20);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                int i = 1 / 0;
                // 业务逻辑处理完成后将缓存的key的value改为1
                redisTemplate.opsForValue().set(messageId, "1", 30, TimeUnit.SECONDS);
                // 手动ack
                System.out.println("手动ACK");
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            } catch (Exception e) {
                System.out.println("消息消费失败,拒绝消息");
                redisTemplate.delete(messageId);
                channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, false);
                System.out.println("投递消息到死信队列");
            }
        } else {
            if ("1".equalsIgnoreCase(redisTemplate.opsForValue().get(messageId))) {
                System.out.println("该消息已经被处理过了，直接手动ACK");
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            }
        }
    }
}