//package com.amqp.hanlder;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.amqp.support.AmqpHeaders;
//import org.springframework.messaging.handler.annotation.Header;
//import org.springframework.stereotype.Component;
//
///**
// * 用于监听某个队列或多个队列数据
// */
//@Component
////@RabbitListener(queues = {"wfx-direct-quence1", "wfx-direct-quence2"})
//@RabbitListener(queues = {"dlx-queue"})
//public class ReceiveDirectHanlder3 {
//
//    @RabbitHandler
//    public void handleMessage(String message,
//                              @Header(AmqpHeaders.CONSUMER_QUEUE) String consumerQueue) {
//        // 消息体
//        System.out.println("死信队列"+consumerQueue+  "收到消息: " + message);
//
//    }
//}