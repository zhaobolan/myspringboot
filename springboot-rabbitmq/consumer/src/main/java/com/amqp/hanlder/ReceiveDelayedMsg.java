package com.amqp.hanlder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 接收延时队列消息
 */
@Component
@RabbitListener(queues = {"delayed.queue"})
public class ReceiveDelayedMsg {

    Logger logger = LoggerFactory.getLogger(ReceiveDelayedMsg.class);

    @RabbitHandler//标记当前方法是用来处理消息的
    public void recevieMsg(String message) {
        logger.info("delayed.queue收到延时消息: 字符串=>" + message);
    }
}