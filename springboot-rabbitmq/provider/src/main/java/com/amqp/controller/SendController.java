package com.amqp.controller;

import com.amqp.service.SendMsgService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 消息发送控制层
 *
 * @author wyj
 * @date 2022/4/26 14:17
 */
@RestController
public class SendController {

    @Resource
    private SendMsgService sendMsgService;

    @GetMapping("/sendString")
    public String sendString(@RequestParam(value = "content") String content, @RequestParam(value = "delayTime") Integer delayTime) {
//        sendMsgService.sendMsgByRabbitMQ(content);
        sendMsgService.sendDelayedMsg(content, delayTime);
        return "success!";
    }
}
