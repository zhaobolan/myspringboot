package com.amqp.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * AmqpTemplate： 这是一个抽象接口，它是 Spring AMQP 提供的一个高层级抽象，用于发送和接收消息的基本操作。它定义了一系列的方法，
 * 如 convertAndSend()、receiveAndConvert() 等，使得开发者可以更容易地与遵循 AMQP 协议的消息中间件交互，而无需关心底层的具体实现细节。
 * 然而，AmqpTemplate 并不特指 RabbitMQ，它可以适用于任何兼容 AMQP 协议的消息中间件。
 * <p>
 * RabbitTemplate： RabbitTemplate 是 AmqpTemplate 接口的一个具体实现，专门为 RabbitMQ 设计和优化的。它不仅实现了 AmqpTemplate
 * 接口中定义的所有基本功能，还提供了针对 RabbitMQ 特性的扩展功能，比如 publisher confirms（生产者确认）、consumer acknowledgments
 * （消费者确认）、消息转换、RabbitMQ 特有的exchange和routing key策略等。
 */
@Service
public class SendMsgService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 通过AmqpTemplate发送字符串消息
     */
    public void sendMsg(String message) {
        // 发送消息到某个队列
        amqpTemplate.convertAndSend("wfx-quence", message);

        // 发送消息到订阅交换机
        amqpTemplate.convertAndSend("fanoutExchange", "", message);
        // 发送消息到路由交换机
        amqpTemplate.convertAndSend("directExchange", "rk1", message);
    }

    /**
     * 通过RabbitTemplate发送对象消息
     */
    public void sendMsgByRabbitMQ(String message) {
        // 设置消息携带的消息ID
        CorrelationData messageId = new CorrelationData(UUID.randomUUID().toString());
        System.out.println("消息ID："+messageId);
        try {
            // 发送消息，并显示开启消息的持久化(默认是开启的)
//            rabbitTemplate.convertAndSend("", "wfx-quence", (Object) message, messageId);
            for (int i = 0; i < 20; i++) {
                message = message+"-" + i;
                rabbitTemplate.convertAndSend("normal-exchange", "info", (Object) message, messageId);
            }
            //TODO  新增消息发送日志记录
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 延时队列发送消息
     */
    public void sendDelayedMsg(String message,Integer delayTime) {
        rabbitTemplate.convertAndSend("delayed.exchange", "delayed.routingkey", message,
                correlationData ->{
                    correlationData.getMessageProperties().setDelay(delayTime);
                    return correlationData;
                });
    }
}