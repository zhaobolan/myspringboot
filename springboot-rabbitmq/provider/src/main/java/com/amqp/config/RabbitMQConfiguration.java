/*
package com.amqp.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfiguration {
    */
/**
     * 普通队列
     *//*

    private static final String QUEUE_NAME = "wfx-quence";
    */
/**
     * 订阅交换机相关
     *//*

    public static final String FANOUT_EXCHANGE = "fanoutExchange";
    private static final String WFX_FANOUT_QUENCE = "wfx-fanout-quence";
    */
/**
     * 路由交换机及相关队列
     *//*

    public static final String DIRECT_EXCHANGE = "directExchange";
    public static final String WFX_DIRECT_QUENCE_1 = "wfx-direct-quence1";
    public static final String WFX_DIRECT_QUENCE_2 = "wfx-direct-quence2";
    public static final String RK_1 = "rk1";
    public static final String RK_2 = "rk2";

    @Bean
    public Queue queue() {
        // 设置死信交换机
        return QueueBuilder.durable(QUEUE_NAME)
                .deadLetterExchange(DIRECT_EXCHANGE)
                .deadLetterRoutingKey(RK_1).build();

    }

    @Bean
    public Queue fanoutQuence() {
        return new Queue(WFX_FANOUT_QUENCE);
    }

    */
/**
     * 声明交换机,fanout 类型(订阅模式)
     *//*

    @Bean
    public FanoutExchange fanoutExchange() {
        FanoutExchange fanoutExchange = new FanoutExchange(FANOUT_EXCHANGE);
        return fanoutExchange;
    }

    */
/**
     * 将队列和交换机绑定
     *//*

    @Bean
    public Binding bindingFanoutExchange(Queue fanoutQuence, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQuence).to(fanoutExchange);
    }

    @Bean
    public Queue directQuence1() {
        return new Queue(WFX_DIRECT_QUENCE_1);
    }

    @Bean
    public Queue directQuence2() {
        return new Queue(WFX_DIRECT_QUENCE_2);
    }

    */
/**
     * 声明交换机,direct 类型(路由模式)
     *//*

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(DIRECT_EXCHANGE);
    }

    */
/**
     * 将队列和交换机绑定
     *//*

    @Bean
    public Binding bindingDirectExchange(Queue directQuence1, DirectExchange directExchange) {
        return BindingBuilder.bind(directQuence1).to(directExchange).with(RK_1);
    }

    @Bean
    public Binding bindingDirectExchange2(Queue directQuence2, DirectExchange directExchange) {
        return BindingBuilder.bind(directQuence2).to(directExchange).with(RK_2);
    }
}

*/
