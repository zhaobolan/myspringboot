package com.amqp.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 死信队列
 */
@Configuration
public class Config {

    /**
     * 正常队列---10s过期
     *
     * @return
     */
    @Bean
    public Queue normalQueue() {
        //ttl(10000):队列里面的消息统一10s过期--x-message-ttl
        return QueueBuilder.durable("normal-queue")
                // 设置队列最大长度
                .maxLength(10)
                // 队列设置过期时间
//                .ttl(10000)
                //绑定死信交换机
                .deadLetterExchange("dlx-exchange")
                //设置路由键
                .deadLetterRoutingKey("error").build();
    }

    /**
     * 死信队列
     *
     * @return
     */
    @Bean
    public Queue dlxQueue() {
        return QueueBuilder.durable("dlx-queue").build();
    }

    /**
     * 正常交换机
     *
     * @return
     */
    @Bean
    public DirectExchange normalExchange() {
        return ExchangeBuilder.directExchange("normal-exchange").build();
    }

    /**
     * 死信交换机
     *
     * @return
     */
    @Bean
    public DirectExchange dlxExchange() {
        return ExchangeBuilder.directExchange("dlx-exchange").build();
    }

    /**
     * 正常队列---10s过期与正常交换机绑定
     *
     * @return
     */
    @Bean
    public Binding dlxBind() {
        return BindingBuilder.bind(normalQueue()).to(normalExchange()).with("info");
    }

    /**
     * 死信队列与死信交换机绑定
     *
     * @return
     */
    @Bean
    public Binding dlxBind1() {
        return BindingBuilder.bind(dlxQueue()).to(dlxExchange()).with("error");
    }

    /**
     * 项目重启时创建交换机与队列
     *
     * @return
     */
//    @Bean
//    public RabbitAdmin dlxRabbitAdmin(ConnectionFactory connectionFactory) {
//        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
//        rabbitAdmin.setAutoStartup(true);
//        rabbitAdmin.declareExchange(dlxExchange());
//        rabbitAdmin.declareExchange(normalExchange());
//        rabbitAdmin.declareQueue(normalQueue());
//        rabbitAdmin.declareQueue(dlxQueue());
//        return rabbitAdmin;
//    }

}
