package com.amqp;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class A {
    public static void main(String[] args) throws Exception {
        publish();
    }

    //生产者
    public static void publish() throws Exception {
        //1、获取connection
        Connection connection = RabbitConfig.getConnection();
        //2、创建channel
        Channel channel = connection.createChannel();
        //3、发送消息到exchange
        String msg = "{\"id\":\"24070410373484511002\"," +
                "\"tenantCode\":\"yfcs104\"," +
                "\"sendTime\":\"2024-07-24\"," +
                "\"content\":[" +
                "{\"deviceId\":\"123\"," +
                "\"alarmTime\":\"2024-07-24\"," +
                "\"dangerItemName\":\"阀门_清灰\"," +
                "\"alarmLevel\":\"1\"," +
                "\"isDisposed\":0," +
                "\"alarmDisposeTime\":\"\"" +
                "}" +
                "]}";
        channel.basicPublish(DangerStationInspectionMqConfig.EXCHANGE,
                String.format(DangerStationInspectionMqConfig.ROUTING_KEY, "yfcs104"),
                null,
                msg.getBytes());
        //PS:exchange是不会将消息持久化的，Queue可以持久化，得配置

        System.out.println("生产者发布消息成功！");
        //4、关闭管道和连接
        channel.close();
        connection.close();
    }
}
