package com.amqp;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DangerStationInspectionMqConfig {
    public static final String EXCHANGE = "danger.station_inspection";
    /**
     * %s：租户编码
     */
    public static final String ROUTING_KEY = "danger.station_inspection.%s";
    /**
     * %s：租户编码
     */
    public static final String QUEUE = "danger.station_inspection.queue.%s";
}