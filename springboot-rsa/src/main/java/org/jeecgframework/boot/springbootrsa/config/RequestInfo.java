package org.jeecgframework.boot.springbootrsa.config;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 请求信息
 * @author jjh
 * @version 1.0
 * @date 2024/11/27 11:29
 */
@Data
public class RequestInfo implements Serializable {
    private String ip;
    private String url;
    private String httpMethod;
    private String classMethod;
    private Object requestParams;
    private String timestamp;
}

