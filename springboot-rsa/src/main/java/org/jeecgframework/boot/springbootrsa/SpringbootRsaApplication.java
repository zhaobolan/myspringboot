package org.jeecgframework.boot.springbootrsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRsaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRsaApplication.class, args);
    }

}
