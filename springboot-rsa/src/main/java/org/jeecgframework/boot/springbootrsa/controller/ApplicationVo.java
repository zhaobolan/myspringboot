package org.jeecgframework.boot.springbootrsa.controller;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: 应用市场Vo
 * @Author: jeecg-boot
 * @Date: 2024-11-08
 * @Version: V1.0
 */
@Data
public class ApplicationVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 应用市场id
     */
    private String appId;
    /**
     * 应用市场名称
     */
    private String appName;
    /**
     * 状态(0.未启用;1.启用)
     */
    private Integer status;

    private Integer pageNo;

    private Integer pageSize;
}
