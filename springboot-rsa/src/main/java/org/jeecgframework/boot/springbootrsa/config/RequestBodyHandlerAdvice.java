package org.jeecgframework.boot.springbootrsa.config;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

@RestControllerAdvice
public class RequestBodyHandlerAdvice implements RequestBodyAdvice {

    @Resource
    private ApiSecurityProperties apiSecurityProperties;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    private static final String NONCE_KEY = "X-AppNonce";

    /**
     * 判断当前的 RequestBodyAdvice 是否支持给定的方法和参数类型
     *
     * @param methodParameter 包含控制器方法的参数信息
     * @param targetType  目标类型，即请求体将要转换成的 Java 类型
     * @param converterType 将要使用的消息转换器的类型
     * @return   返回 true 表示支持，false 表示不支持
     */
    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return methodParameter.hasMethodAnnotation(ApiSecurity.class)
                || AnnotatedElementUtils.hasAnnotation(methodParameter.getDeclaringClass(), ApiSecurity.class);
    }

    /**
     * 接口入参解密(在请求体被反序列化之前调用，可以对请求体进行预处理)
     * @param inputMessage 包含 HTTP 请求的头和体
     * @param parameter  包含控制器方法的参数信息
     * @param targetType  目标类型，即请求体将要转换成的 Java 类型
     * @param converterType  将要使用的消息转换器的类型
     * @return   返回新的流
     */
    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        ApiSecurity apiSecurity = parameter.getMethodAnnotation(ApiSecurity.class);
        // 判断接口参数是否需要解密
        Boolean enable = apiSecurityProperties.getEnable();
        boolean decryptRequest = apiSecurity.decryptRequest();
        if (!decryptRequest || !enable) {
            return inputMessage;
        }
        InputStream inputStream = inputMessage.getBody();
        String body = IoUtil.read(inputStream, StandardCharsets.UTF_8);
        if (StrUtil.isBlank(body)) {
            throw new BizException("请求参数body不能为空");
        }
        // 加密传参格式固定为ApiSecurityParam
        ApiSecurityParam apiSecurityParam = JSON.parseObject(body, ApiSecurityParam.class);
        // 通过RSA私钥解密获取到aes秘钥
        String aesKey = RSAUtil.decryptByPrivateKey(apiSecurityParam.getKey(), apiSecurityProperties.getRsaPrivateKey());
        // 通过aes秘钥解密data参数数据，即真正实际的接口参数
        String data = AESUtil.decrypt(apiSecurityParam.getData(), aesKey);

        // 验签
        if (apiSecurity.isSign()) {
            verifySign(apiSecurityParam, aesKey);
        }
        // 使用解密后的数据构造新的读取流, Spring MVC后续读取解析转换为接口
        apiSecurityParam.setData(data);
        return new PtcHttpInputMessage(inputMessage.getHeaders(), JSONObject.toJSONString(apiSecurityParam));
    }

    /**
     * 在请求体被反序列化之后，但在调用控制器方法之前调用，可以对目标对象进行后处理
     */
    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    /**
     * 处理body为空的这种情况，比如当body位空时，返回一个默认对象啥的
     */
    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter,
                                  Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return null;
    }

    void verifySign(ApiSecurityParam apiSecurityParam, String aesKey) {
        // 如果请求参数是加密传输的，先从ApiSecurityParam获取签名和时间戳放到headers里面这里读取。
        // 如果请求参数是非加密即明文传输的，那签名参数只能放到header中

        String nonce = apiSecurityParam.getNonce();
        if (StrUtil.isBlank(nonce)) {
            throw new BizException("唯一标识不能为空");
        }

        String timestamp = apiSecurityParam.getTimestamp();
        if (StrUtil.isBlank(timestamp)) {
            throw new BizException("时间戳不能为空");
        }
        String sign = apiSecurityParam.getSign();
        if (StrUtil.isBlank(sign)) {
            throw new BizException("签名不能为空");
        }
        try {
            long time = Long.parseLong(timestamp);
            // 判断timestamp时间戳与当前时间是否超过签名有效时长（过期时间根据业务情况进行配置）,如果超过了就提示签名过期
            long now = System.currentTimeMillis() / 1000;
            if (now - time > apiSecurityProperties.getValidTime()) {
                throw new BizException("签名已过期");
            }
        } catch (Exception e) {
            throw new BizException("非法的时间戳");
        }
        // 验签
        SortedMap sortedMap = SignUtil.beanToMap(apiSecurityParam);
        String content = SignUtil.getContent(sortedMap, nonce, timestamp);
        String encrypt = AESUtil.encrypt(content, aesKey);
        if (!encrypt.equalsIgnoreCase(sign)) {
            throw new BizException("签名验证不通过");
        }

        // 判断nonce
        if (stringRedisTemplate.hasKey(NONCE_KEY + nonce)) {
            //请求重复
            throw new BizException("唯一标识nonce已存在");
        }
        stringRedisTemplate.opsForValue().set(NONCE_KEY + nonce, "1", apiSecurityProperties.getValidTime(), TimeUnit.SECONDS);
    }
}
