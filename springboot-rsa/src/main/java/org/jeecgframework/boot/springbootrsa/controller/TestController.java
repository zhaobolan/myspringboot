package org.jeecgframework.boot.springbootrsa.controller;

import com.alibaba.fastjson.JSONObject;
import org.jeecgframework.boot.springbootrsa.config.ApiSecurity;
import org.jeecgframework.boot.springbootrsa.config.ApiSecurityParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {


    @PostMapping("/test")
    @ApiSecurity(encryptResponse = true, decryptRequest = true, isSign = false)
    public ApplicationVo test(@RequestBody ApiSecurityParam apiSecurityParam) {
        String data = apiSecurityParam.getData();
        ApplicationVo applicationVo = JSONObject.parseObject(data, ApplicationVo.class);
        return applicationVo;
    }
}
