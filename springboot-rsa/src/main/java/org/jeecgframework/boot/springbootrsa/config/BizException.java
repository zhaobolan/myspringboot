package org.jeecgframework.boot.springbootrsa.config;

/**
 * @author jjh
 * @version 1.0
 * @date 2024/11/27 14:40
 */

import lombok.Data;

/**
 * 业务异常类
 */
@Data
public class BizException extends RuntimeException {

    private Integer code;

    public BizException() {
        super();
    }

    public BizException(String message) {
        super(message);
    }

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public BizException(ResponseStatusEnum responseStatusEnum) {
        super(responseStatusEnum.getMsg());
        this.code = responseStatusEnum.getCode();
    }

}
