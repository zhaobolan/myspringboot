package org.jeecgframework.boot.springbootrsa.config;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

/**
 * @author jjh
 * @version 1.0
 * @date 2024/11/27 15:10
 */
@Slf4j
public class RSAUtil {
    public static final String KEY_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "SHA1WithRSA";
    public static final String ENCODING = "utf-8";
    public static final String X509 = "X.509";

    public static KeyPair getKeyPair(int keyLength) {
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");   //默认:RSA/None/PKCS1Padding
            keyPairGenerator.initialize(keyLength);
            return keyPairGenerator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("生成密钥对时遇到异常" +  e.getMessage());
        }
    }

    public static byte[] getPublicKey(KeyPair keyPair) {
        RSAPublicKey rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        return rsaPublicKey.getEncoded();
    }

    public static byte[] getPrivateKey(KeyPair keyPair) {
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyPair.getPrivate();
        return rsaPrivateKey.getEncoded();
    }

    /**
     * 获取公钥
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static PublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    /**
     * 获取私钥
     *
     * @param key
     * @return
     * @throws Exception
     */
    public static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(key.getBytes(ENCODING));
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }

    /**
     * RSA私钥签名
     *
     * @param content    待签名数据
     * @param privateKey 私钥
     * @return 签名值
     */
/*    public static String signByPrivateKey(String content, String privateKey) {
        try {
            PrivateKey priKey = getPrivateKey(privateKey);
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initSign(priKey);
            signature.update(content.getBytes(ENCODING));
            byte[] signed = signature.sign();
            return new String(Base64.encodeBase64URLSafe(signed), ENCODING);
        } catch (Exception e) {
            log.warn("sign error, content: {}, priKey: {}", new Object[]{content, privateKey});
            log.error("sign error", e);
        }
        return null;
    }*/

    /**
     * RSA公钥验签
     *
     * @param content   待验签数据
     * @param publicKey 公钥
     * @return 是否成功
     */
/*    public static boolean verifySignByPublicKey(String content, String sign, String publicKey) {
        try {
            PublicKey pubKey = getPublicKey(publicKey);
            Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
            signature.initVerify(pubKey);
            signature.update(content.getBytes(ENCODING));
            return signature.verify(Base64.decodeBase64(sign.getBytes(ENCODING)));
        } catch (Exception e) {
            log.warn("sign error, content: {}, sign: {}, pubKey: {}", content, sign, publicKey);
            log.error("sign error", e);
        }
        return false;
    }*/

    /**
     * 通过公钥对aes进⾏加密
     *
     * @param plainText 随机⽣成的aes key
     * @param publicKey 公钥
     * @return RSA加密后的aes key
     */
    public static String encryptByPublicKey(String plainText, String publicKey) {
        try {
            PublicKey pubKey = getPublicKey(publicKey);
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] enBytes = cipher.doFinal(plainText.getBytes(ENCODING));
            return Base64.encodeBase64String(enBytes);
        } catch (Exception e) {
            log.error("encrypt error", e);
        }
        return null;
    }

    /**
     * 通过私钥对aes进⾏解密
     *
     * @param enStr      加密后的aes key
     * @param privateKey 西撕私钥
     * @return RSA加密后的aes key
     */
    public static String decryptByPrivateKey(String enStr, String privateKey) {
        try {
            PrivateKey priKey = getPrivateKey(privateKey);
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, priKey);
            byte[] deBytes = cipher.doFinal(Base64.decodeBase64(enStr));
            return new String(deBytes);
        } catch (Exception e) {
            log.error("decrypt error", e);
        }
        return null;
    }

    public static void main(String[] args) {
        String publicKeyString = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA5FM3oYiA+aFTsFI+4Y+Tm020pa1nCVQpy9EJVawbY8NaZ298POwoNKyUAZnPC0oJ8AfMBbGYHutzYkY8JccP6EUg/XuG7R9l1qAaMK2lnyWoWt8XgKBXg1Hz07NZ2wOdwNqeVlFKwNqBsAc0ElxkNXNYuWXbE58iiweGJP+XvUYXhMe5RUPsrj3epuv1hvqWH2p4q6GkaO45e8vBx/+6/xzjzqbzd1KZxBWG45UST+onS/5o/YSoC/bYWAIyRmDKZrUG5US+AOfM6zQ8lJoQcVrZsjtKJT3ihTM44OVETZ1aFD9MwUCEr9nxOQAId37347Pm7cRZnfJBAPLNggkiXl2oO4cbuDsEfegfF4QUVF/f72i757cy4/ao/nCkzJRVJMu+exdO3S2qdJsBbpH+9+zHOuTSZ+vbfmEvY4dpii733fWV5mPxY0RSbtV1CO96CsBJbSxW71N2IJFjUdwCd3pilKhZg3XiMTCso+fazpcrt51SxEKmzCLWaGIGh2k9LMgKkjy/EQ1gc0r/DhIpbcxSjYnamDlt71Kib8MP+87J8cCzxokzVdbzCjC1jfdrMo3KxfdVkOquHpFKHIF1yahs7FgE2bFU6rTR781AYHhR8tbVxfuB6mpwhiIG7LQJp7WP7fwfi/IK1I/YdKZl872Ftm4GPpDZPkf7i5TXgicCAwEAAQ==";
        String privateKeyString = "MIIJRQIBADANBgkqhkiG9w0BAQEFAASCCS8wggkrAgEAAoICAQDkUzehiID5oVOwUj7hj5ObTbSlrWcJVCnL0QlVrBtjw1pnb3w87Cg0rJQBmc8LSgnwB8wFsZge63NiRjwlxw/oRSD9e4btH2XWoBowraWfJaha3xeAoFeDUfPTs1nbA53A2p5WUUrA2oGwBzQSXGQ1c1i5ZdsTnyKLB4Yk/5e9RheEx7lFQ+yuPd6m6/WG+pYfaniroaRo7jl7y8HH/7r/HOPOpvN3UpnEFYbjlRJP6idL/mj9hKgL9thYAjJGYMpmtQblRL4A58zrNDyUmhBxWtmyO0olPeKFMzjg5URNnVoUP0zBQISv2fE5AAh3fvfjs+btxFmd8kEA8s2CCSJeXag7hxu4OwR96B8XhBRUX9/vaLvntzLj9qj+cKTMlFUky757F07dLap0mwFukf737Mc65NJn69t+YS9jh2mKLvfd9ZXmY/FjRFJu1XUI73oKwEltLFbvU3YgkWNR3AJ3emKUqFmDdeIxMKyj59rOlyu3nVLEQqbMItZoYgaHaT0syAqSPL8RDWBzSv8OEiltzFKNidqYOW3vUqJvww/7zsnxwLPGiTNV1vMKMLWN92syjcrF91WQ6q4ekUocgXXJqGzsWATZsVTqtNHvzUBgeFHy1tXF+4HqanCGIgbstAmntY/t/B+L8grUj9h0pmXzvYW2bgY+kNk+R/uLlNeCJwIDAQABAoICAQCnPsGgJAH9WIuZuxTuWiIP/2JUfWjc73ZSIkNJKvFBKUZeP85sPbvRoZwkdAqxxUD0PnHWtdisxWDWWoYSUfa393ML2eDibj5FcS7d+0Q3B/mC3qkzgb+4pF4K8vPdto9mRXz300IDc8rNKumcjWqlsGEMJKzNeaJu3ksHbOU0MlRl4Xo3X2LN2untsSc/AOzYBEBxz8xDRER9BqpiGlNaLUo9BBOQksOpWu9cYsRwHx87zhYPpY0GfoDILouu+5UmmHF9nNXkieaTXscFPrjOn3KY2E8eam8zjGqcabOXeqwR6rAJWn4W5XiJfRm2lh10AEImZc4TIPE2haO9HX266QlgL2mR1CfIt3k4Z1kdMD7SFPdFoF4smXbnYvcNtyP9O090XFsOOyQVLq/hRF5pdSwxARMbFnxd0UdVxRWvGxOEwJ2PaPSHdfQptFdNVVXhibTx7BQ+F2JPa1d1sfWEMPmwny9XmjnatXW81ItAe6IrAJP/qKLyM+oPDwt0QO346CTTZTna2hPma2Ib/vD/3/VDL9/3hWVH8CQjGpokJH4HGtHifd6rQtXWMz7hyUmDPnmdlWqW721hucPiSxQo0wGcLFASHN6LjKdzIg3jj5lJBgI6KYfLgQIQWU9VCRIwBMmYpFqCGjRG1mF1uOddFl4hzvQzhn0r8jZ3AyHbUQKCAQEA/sJDcuHyIjnoxrKCsP/psfjVEmlquHB3GxJxNBUS2USHD77hIEil+HGvykfOGQHYdmkLLJ+t2wML8hvRyxopwGTinIvkQcm5g51aO0P56qSVn+qrIeINiAt9ELT2uzhzJGQc5aHxdC33cjaIi2x771ArcxyatQq+/vUrk6UAqz7xtAPFJoZoTRZjmXGKYoyFsopqmqO6GekqZXLxFeVTH/P9cY5olMf9f8C1Z53+wbrK92WouIW8mKWdHe3Y9rNbnfW+WfAh+7eMA07K8xO4Jdb5D+5efSLG+HVB9NLfns1fM/17+lUTJfmjhBIXyZ/dH5pRhMjEuFpjNaQGh18P+QKCAQEA5W/8SaaX25gthMYtkXkg8keSMqY94NlXIO1x3TbfWjkmCz1nnSYUygKJvtE63wcgzDublDDOYA0CtXEO1cFy+avGWvOvaJtJQkekndrvoHLOoO991aES5pszM5iu8O0jR4bdzdHyiEC9QQK9yQmxdmHH41Vn4nYllL3JDWanphBPTKw58cwP5Ir4HwVvliNlG0ju1RwMCVG/r/3S86NTNg2CDiJWGdPxBUuXpb714Xug7k/YyHHPhVaTo9OnXHU476qAWNU1ET6NBgVUMEm73prynQ0Kx31VHEaBK7/KFGx4QCmJYX38moYMbBwidLREKQS9bJCozsqfHPkmUsPrHwKCAQEAqOmrCPfwsGrPATeI2czEaUFAHTYSEXpYLnX64vp6Bd6e4nzADcS9bs4KLUeQyhnkKRaa1xqEjmslt4BkiJKvTiZV7k/+pvQWccKZ+iOnB8Hahy50C0IzkJl1cedalGXoSh+q3UKTsmFr6KlpvW/PeKwiMu+rUBBw9OEf5IQJvoE8hwvc4x9I9pu6rBmRFKhxWSDXb/LQyK9FcgBVAxyLEeuXhnxSsRss5FCktrAWIuM0AfbAeKALnPZqB1cVfPUM8N3oOP+zuI9JYUQT4jmX5lbzynFU6wls/X3ZnndW15b54aHXWvKfgJVVqpLIfOHnHjYWiMEJVeZwUzePMKCCGQKCAQEAuTfAKQzNxr6A8JCztJ5eVHgms7C0f1G1HqXQp85mXIWKCsTVYm2dlr6ZyGtL5UPx3C26mNUVpUCbkTHl0fXLBXcAN+hFEGNbrp/7aluV2qcT26O6p7zSb0uW9+4oO+KT+mgLot37trbckSDPaP3kPzcMkJ8fsBfEmuDi72iRqmO8QJ9AX8xmgA8X4qMWw9NrCa3E8FEl/lf6MO7tXzXYNOiOty5M56jLB4VQtMMF57NP5VKcJKSL7I5JvZVdpCwVfJTd69iicHtYiSXAIwNDIJ9hgBtJOmYZUSwxlsnLLtWp9dIST5vPUkcovBWyUxg7dgmXiY72V4oHDPKtV19F7QKCAQEAxKphJXTO/ZR4/1kSW2RIFZ8evgLUcXMrrDZNAZ/KHiI8iQa2tJtpNT1EgtEFMGevRUhQLvRexog9KnawLgsVvs6yV2gKhzXvsjg7zvbx0ZhzVBTEhbD2E86WdRnBP8n0rxcUIOMcMgGWGKDA4jPKch7Mpwo+PBrl/vbk3WhfsNWQGrEec620J8w9KYdDQ8v9WzYTs90cFMS+meX5NNXud4gKKvvue0Lnkk4uC0AIENaTnqkpCOaf19H6pEuPLoh4FpMn6Q7KbnLbawG/Fwow+7P9vEriEItf+XZZ834lC9U46Y0R+QaG6DZWQjrl8shOHpe5LuKY3sSAcI7mpgW0rw==";
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setIp("127.0.0.1");
        requestInfo.setUrl("/test/1");
        requestInfo.setHttpMethod("12");
        requestInfo.setTimestamp(String.valueOf(new Date().getTime()));
        String s = JSONObject.toJSONString(requestInfo);
//        String sign = signByPrivateKey(s, privateKeyString);
    }
}
