package org.jeecgframework.boot.springbootrsa.config;

import cn.hutool.core.io.IoUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;

import java.io.InputStream;

/**
 * @author jjh
 * @version 1.0
 * @date 2024/11/27 14:32
 */
public class PtcHttpInputMessage implements HttpInputMessage {
    private HttpHeaders headers;
    private String body;

    public PtcHttpInputMessage(HttpHeaders headers, String body) {
        this.headers = headers;
        this.body = body;
    }

    @Override
    public InputStream getBody() {
        return IoUtil.toUtf8Stream(body);
    }

    @Override
    public HttpHeaders getHeaders() {
        return this.headers;
    }
}
