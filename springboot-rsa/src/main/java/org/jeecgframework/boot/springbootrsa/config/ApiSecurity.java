package org.jeecgframework.boot.springbootrsa.config;

import cn.hutool.core.annotation.Alias;

import java.lang.annotation.*;

/**
 * @author jjh
 * @version 1.0
 * @date 2024/11/27 11:50
 *
 * 该注解用于标识 需要经过加密或者加签来加固接口安全性的接口
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface ApiSecurity {

    /**
     * 是否加签验证，默认开启
     */
    boolean isSign() default true;

    /**
     * 接口请求参数是否需要解密
     */
    boolean decryptRequest() default false;

    /**
     * 接口响应参数是否需要加密
     */
    boolean encryptResponse() default false;
}
