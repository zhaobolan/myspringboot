package com.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2 // 启用swagger注解,默认访问地址：http://localhost:8081/swagger-ui.html#
@SpringBootApplication
public class SpringbootFastdfsApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootFastdfsApplication.class, args);
    }

}
