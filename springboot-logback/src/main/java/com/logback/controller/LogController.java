package com.logback.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class LogController {


    @RequestMapping("/testLog")
    public String testLog(String msg) {
        log.info("这是info信息：{}", msg);
        log.debug("这是debug信息：{}", msg);
        log.warn("这是warn信息：{}", msg);
        log.error("这是error信息：{}", msg);
        return "success";
    }
}
