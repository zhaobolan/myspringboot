package com.postgresql.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {

    @Autowired
    @Qualifier("jdbc_pgsql")
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = "/getInfo")
    public Object get() {
        String sql = "SELECT * FROM company WHERE id =?";
        return jdbcTemplate.queryForList(sql, 1);
    }


}

