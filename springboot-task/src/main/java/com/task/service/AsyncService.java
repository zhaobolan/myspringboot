package com.task.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @description:
 * @author: wyj
 * @time: 2020/6/28 15:58
 */
@Service
public class AsyncService {

    @Async  //指定此方法为异步调用
    public void  hello() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("加载中。。。。。。。");
    }

    //5 * * 28 * 7写法可参考: https://blog.csdn.net/fanrenxiang/article/details/80361582
    @Scheduled(cron = "5 * * 28 * 7")  //指定此方法为定时任务
    public void timing() {
        System.out.println("我爱我的母亲。。。。");
    }

}
