package com.task;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SpringbootTaskApplicationTests {

    @Autowired
    JavaMailSender javaMailSender;

    @Test
    void contextLoads() {
        // 简单邮件发送
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        // 设置邮箱
        simpleMailMessage.setSubject("紧急通知");   // 标题
        simpleMailMessage.setText("     请所有员工晚上7点开会,所有人不许迟到！"); // 内容
        simpleMailMessage.setTo("804599591@qq.com");
        simpleMailMessage.setFrom("351094262@qq.com");  // 必须设置发件人
        javaMailSender.send(simpleMailMessage);
    }

    @Test
    void Test01() throws Exception{
        // 复杂邮件发送
        MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
        // 上传文件需设置multipart参数为true
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage,true);

        // 设置邮箱
        mimeMessageHelper.setSubject("紧急通知");   // 标题
        mimeMessageHelper.setText("<b style='color:red'>请所有员工晚上7点开会,所有人不许迟到！</b>",true); // 内容,可自定义样式
        mimeMessageHelper.setTo("804599591@qq.com");
        mimeMessageHelper.setFrom("351094262@qq.com");  // 必须设置发件人
        // 上传文件
        mimeMessageHelper.addAttachment("剑帝.jpg",new File("C:\\Users\\35109\\Pictures\\Saved Pictures\\剑帝.jpg"));
        mimeMessageHelper.addAttachment("68751856.jpeg",new File("C:\\Users\\35109\\Pictures\\Saved Pictures\\68751856.jpeg"));
        javaMailSender.send(mimeMailMessage);
    }

}
