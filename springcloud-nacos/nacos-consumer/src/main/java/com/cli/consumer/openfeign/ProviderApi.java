package com.cli.consumer.openfeign;

import com.cli.consumer.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "provider")
public interface ProviderApi {

    @GetMapping("/get")
    ResponseEntity<String> get();

    @PostMapping("getUser")
    ResponseEntity<User> getUser(@RequestBody User user);
}
