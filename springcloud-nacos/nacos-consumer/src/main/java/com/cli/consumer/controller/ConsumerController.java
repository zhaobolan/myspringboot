package com.cli.consumer.controller;

import com.cli.consumer.entity.User;
import com.cli.consumer.openfeign.ProviderApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ConsumerController {

    private final ProviderApi providerApi;

    @GetMapping("/test")
    public String get() {
        // 从提供者中获取数据
        ResponseEntity<String> stringResponseEntity = providerApi.get();
        return stringResponseEntity.getBody();
    }

    @GetMapping("/testLog")
    public String testLog(String msg) {
        log.debug("这是debug信息：{}", msg);
        log.info("这是info信息：{}", msg);
        log.warn("这是warn信息：{}", msg);
        log.error("这是error信息：{}", msg);
        return "success";
    }

    @GetMapping("/getUser")
    public String getUser(@RequestParam("age") Integer age) {
        User user = new User();
        user.setAge(age);
        user.setBirthday(LocalDate.now());

        ResponseEntity<User> user1 = providerApi.getUser(user);

        User body = user1.getBody();

        return body.toString();
    }


}
