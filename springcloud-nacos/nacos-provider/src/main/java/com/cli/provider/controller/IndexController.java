package com.cli.provider.controller;

import com.cli.provider.entity.User.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class IndexController {

    @GetMapping("get")
    public String get() {
        return "喵喵喵";
    }


    @PostMapping("getUser")
    public User getUser(@RequestBody User user) {
        // 模拟构造10个User对象数据的集合
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user1 = new User();
            user1.setName("小明" + i);
            user1.setAge(i + 20);
            user1.setBirthday(LocalDate.now());
            user1.setDateTime(LocalDateTime.now());
            userList.add(user1);
        }

        User get = userList.stream()
                .filter(o -> o.getAge() == user.getAge() && o.getBirthday().equals(user.getBirthday()))
                .findAny().orElseGet(() -> new User("魏义已", 20, LocalDate.now(), LocalDateTime.now()));
        log.info("查询用户结果:{}", get.toString());
        return get;
    }
}
