package com.mybatisplus.util;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

/**
 * mybatisplus代码生成器
 */
public class GeneratorCodeConfig {
    /**
     * 数据库连接信息
     */
    public static final String AUTHOR = "weiyiji";
//    public static final String URL = "jdbc:mysql://192.168.211.131:3306/demo?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=Asia/Shanghai";
//    public static final String USERNAME = "root";
//    public static final String PASSWORD = "123456";
    public static final String URL = "jdbc:postgresql://172.201.1.22:5432/gis_yfcs104";
    public static final String USERNAME = "gmis_gis";
    public static final String PASSWORD = "123456";

    /**
     * 指定表名
     */
    public static final String[] TABLENAME = {"mob_cockpit_danger_daily","mob_cockpit_danger_accumulate_summary_1d"};
    /**
     * 表前缀
     */
    public static final String[] TABLE_PREFIX = {};

    public static void main(String[] args) {
        FastAutoGenerator.create(URL, USERNAME, PASSWORD)
                .globalConfig(builder -> builder
                        .author(AUTHOR)
                        .outputDir("C:\\Users\\Administrator\\Desktop\\dwps-pgdata")
                )
                .packageConfig(builder -> builder
                        .parent("com.cdqckj.dwps.pgdata")
                        .entity("entity")
                        .mapper("dao")
                        .service("service")
                        .controller("controller")
                        .serviceImpl("service.impl")
                        .xml("mapper")
                )
                .strategyConfig(builder -> {
                            builder.addInclude(TABLENAME)
                                    .addTablePrefix(TABLE_PREFIX)
                                    .entityBuilder()
                                    .enableLombok()
                                    .controllerBuilder()
                                    .enableRestStyle();
                        }
                )
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}