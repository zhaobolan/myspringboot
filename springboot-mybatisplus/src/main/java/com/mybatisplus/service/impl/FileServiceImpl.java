package com.mybatisplus.service.impl;

import com.mybatisplus.entity.File;
import com.mybatisplus.mapper.FileMapper;
import com.mybatisplus.service.FileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 * 文件表 服务实现类
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, File> implements FileService {


    @Override
    public Map<String, File> getFileMap() {
        return baseMapper.getFileMap();
    }
}
