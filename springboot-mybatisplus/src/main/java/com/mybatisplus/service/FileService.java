package com.mybatisplus.service;

import com.mybatisplus.entity.File;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 文件表 服务类
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
public interface FileService extends IService<File> {

    Map<String,File> getFileMap();
}
