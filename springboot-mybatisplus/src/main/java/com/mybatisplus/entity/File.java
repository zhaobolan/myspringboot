package com.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 文件表
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
@Getter
@Setter
@TableName("f_file")
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId("id")
    private Long id;

    /**
     * 数据类型
     * #DataType{DIR:目录;IMAGE:图片;VIDEO:视频;AUDIO:音频;DOC:文档;OTHER:其他}
     */
    @TableField("data_type")
    private String dataType;

    /**
     * 原始文件名
     */
    @TableField("submitted_file_name")
    private String submittedFileName;

    /**
     * 父目录层级关系
     */
    @TableField("tree_path")
    private String treePath;

    /**
     * 层级等级
     * 从1开始计算
     */
    @TableField("grade")
    private Integer grade;

    /**
     * 是否删除
     * #BooleanStatus{TRUE:1,已删除;FALSE:0,未删除}
     */
    @TableField("is_delete")
    private Boolean isDelete;

    /**
     * 父文件夹ID
     */
    @TableField("folder_id")
    private Long folderId;

    /**
     * 文件访问链接
     * 需要通过nginx配置路由，才能访问
     */
    @TableField("url")
    private String url;

    /**
     * 文件大小
     * 单位字节
     */
    @TableField("size")
    private Long size;

    /**
     * 父文件夹名称
     */
    @TableField("folder_name")
    private String folderName;

    /**
     * FastDFS组
     * 用于FastDFS
     */
    @TableField("group_")
    private String group;

    /**
     * FastDFS远程文件名
     * 用于FastDFS
     */
    @TableField("path")
    private String path;

    /**
     * 文件的相对路径
     */
    @TableField("relative_path")
    private String relativePath;

    /**
     * md5值
     */
    @TableField("file_md5")
    private String fileMd5;

    /**
     * 文件类型
     * 取上传文件的值
     */
    @TableField("context_type")
    private String contextType;

    /**
     * 唯一文件名
     */
    @TableField("filename")
    private String filename;

    /**
     * 文件名后缀
     * (没有.)
     */
    @TableField("ext")
    private String ext;

    /**
     * 文件图标
     * 用于云盘显示
     */
    @TableField("icon")
    private String icon;

    /**
     * 创建时年月
     * 格式：yyyy-MM 用于统计
     */
    @TableField("create_month")
    private String createMonth;

    /**
     * 创建时年周
     * yyyy-ww 用于统计
     */
    @TableField("create_week")
    private String createWeek;

    /**
     * 创建时年月日
     * 格式： yyyy-MM-dd 用于统计
     */
    @TableField("create_day")
    private String createDay;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 创建人
     */
    @TableField("create_user")
    private Long createUser;

    /**
     * 秦川修改时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 秦川修改人
     */
    @TableField("update_user")
    private Long updateUser;
}
