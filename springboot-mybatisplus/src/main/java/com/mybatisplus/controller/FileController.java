package com.mybatisplus.controller;

import com.mybatisplus.entity.File;
import com.mybatisplus.service.FileService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 文件表 前端控制器
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @Resource
    private FileService fileService;

    @GetMapping("/test")
    public String test() {
        return fileService.getById(1L).toString();
    }

    @GetMapping("/testMap")
    public String testMap() {
        Map<String, File> fileMap = fileService.getFileMap();
        for (Map.Entry<String, File> stringFileEntry : fileMap.entrySet()) {
            System.out.println(stringFileEntry.getKey() + ":" + stringFileEntry.getValue().getId());
        }

        return fileService.getFileMap().toString();
    }
}
