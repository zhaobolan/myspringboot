package com.mybatisplus.mapper;

import com.mybatisplus.entity.File;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.MapKey;

import java.util.Map;

/**
 * <p>
 * 文件表 Mapper 接口
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
public interface FileMapper extends BaseMapper<File> {

    // 测试MapKey注解，测试结果，Map的key值如果在数据库中存在多行得话，会按顺序去最后得一条
    @MapKey("path")
    Map<String,File> getFileMap();
}
