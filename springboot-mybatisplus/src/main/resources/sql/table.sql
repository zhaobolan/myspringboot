CREATE TABLE `f_file` (
                          `id` bigint NOT NULL COMMENT '主键',
                          `data_type` varchar(255) DEFAULT 'IMAGE' COMMENT '数据类型\n#DataType{DIR:目录;IMAGE:图片;VIDEO:视频;AUDIO:音频;DOC:文档;OTHER:其他}',
                          `submitted_file_name` varchar(255) DEFAULT '' COMMENT '原始文件名',
                          `tree_path` varchar(255) DEFAULT ',' COMMENT '父目录层级关系',
                          `grade` int DEFAULT '1' COMMENT '层级等级\n从1开始计算',
                          `is_delete` bit(1) DEFAULT b'0' COMMENT '是否删除\n#BooleanStatus{TRUE:1,已删除;FALSE:0,未删除}',
                          `folder_id` bigint DEFAULT '0' COMMENT '父文件夹ID',
                          `url` varchar(1000) DEFAULT '' COMMENT '文件访问链接\n需要通过nginx配置路由，才能访问',
                          `size` bigint DEFAULT '0' COMMENT '文件大小\n单位字节',
                          `folder_name` varchar(255) DEFAULT '' COMMENT '父文件夹名称',
                          `group_` varchar(255) DEFAULT '' COMMENT 'FastDFS组\n用于FastDFS',
                          `path` varchar(255) DEFAULT '' COMMENT 'FastDFS远程文件名\n用于FastDFS',
                          `relative_path` varchar(255) DEFAULT '' COMMENT '文件的相对路径 ',
                          `file_md5` varchar(255) DEFAULT '' COMMENT 'md5值',
                          `context_type` varchar(255) DEFAULT '' COMMENT '文件类型\n取上传文件的值',
                          `filename` varchar(255) DEFAULT '' COMMENT '唯一文件名',
                          `ext` varchar(64) DEFAULT '' COMMENT '文件名后缀 \n(没有.)',
                          `icon` varchar(64) DEFAULT '' COMMENT '文件图标\n用于云盘显示',
                          `create_month` varchar(10) DEFAULT NULL COMMENT '创建时年月\n格式：yyyy-MM 用于统计',
                          `create_week` varchar(10) DEFAULT NULL COMMENT '创建时年周\nyyyy-ww 用于统计',
                          `create_day` varchar(12) DEFAULT NULL COMMENT '创建时年月日\n格式： yyyy-MM-dd 用于统计',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `create_user` bigint DEFAULT NULL COMMENT '创建人',
                          `update_time` datetime DEFAULT NULL COMMENT '秦川修改时间',
                          `update_user` bigint DEFAULT NULL COMMENT '秦川修改人',
                          PRIMARY KEY (`id`) USING BTREE,
                          FULLTEXT KEY `FU_TREE_PATH` (`tree_path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='文件表';


INSERT INTO `demo`.`f_file` (`id`, `data_type`, `submitted_file_name`, `tree_path`, `grade`, `is_delete`, `folder_id`, `url`, `size`, `folder_name`, `group_`, `path`, `relative_path`, `file_md5`, `context_type`, `filename`, `ext`, `icon`, `create_month`, `create_week`, `create_day`, `create_time`, `create_user`, `update_time`, `update_user`) VALUES (1, 'IMAGE', '', ',', 1, b'0', 0, '', 0, '', '', '/home', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
