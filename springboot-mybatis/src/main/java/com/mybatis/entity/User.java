package com.mybatis.entity;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;
import org.locationtech.jts.geom.Geometry;

@Data
public class User extends BaseEntity{
    private Long id;

    private String nickname;

    private String mobile;

    private String password;

    private String role;

    private JsonNode details;

    private Geometry geom;
}