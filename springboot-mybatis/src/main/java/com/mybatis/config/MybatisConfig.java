package com.mybatis.config;

import com.mybatis.config.handler.json.JsonNodeTypeHandler;
import com.mybatis.config.handler.jts.JtsGeometryTypeHandler;
import com.mybatis.config.interceptor.BaseEntityInterceptor;
import com.mybatis.config.interceptor.SqlInterceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.util.Arrays;

/**
 * Mybatis自定义配置类
 *
 * @author wyj
 * @date 2023/4/6 10:45
 */
@Configuration
public class MybatisConfig {

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);

        // 添加xml文件的映射路径(包含原始的和自己扩展的)
        Resource[] resources = new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/*.xml");
        Resource[] resources1 = new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/ex/*.xml");
        Resource[] concat = concat(resources, resources1);
        bean.setMapperLocations(concat);

        // 设置自定义类型处理器(全局有效)
        bean.setTypeHandlers(new JsonNodeTypeHandler(),new JtsGeometryTypeHandler());

        // 设置自定义拦截器(全局有效)
//        bean.setPlugins(new SqlInterceptor(), new BaseEntityInterceptor());
        return bean.getObject();
    }

    /**
     * 将2个数组合并成一个新的数组
     *
     * @param first
     * @param second
     * @return 响应结果
     * @date 2023/4/6 11:14
     */
    private static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
