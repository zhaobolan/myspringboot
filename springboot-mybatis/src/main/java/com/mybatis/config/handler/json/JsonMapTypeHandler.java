package com.mybatis.config.handler.json;

import org.apache.ibatis.type.MappedTypes;

import java.util.Map;

@MappedTypes({Object.class})
public class JsonMapTypeHandler extends AbstractMysqlJsonTypeHandler<Map<String,Object>> {

}
