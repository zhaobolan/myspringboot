package com.mybatis.config.handler.json;

import org.apache.ibatis.type.MappedTypes;

import java.util.List;
import java.util.Map;

@MappedTypes({Object.class})
public class JsonListMapTypeHandler extends AbstractMysqlJsonTypeHandler<List<Map<String, Object>>> {

}
