package com.mybatis.config.handler.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


@MappedJdbcTypes(value = {JdbcType.OTHER}, includeNullJdbcType = true)
public abstract class AbstractMysqlJsonTypeHandler<T> extends BaseTypeHandler<T> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public ObjectMapper objectMapper;

    private TypeReference<T> typeReference;

    public AbstractMysqlJsonTypeHandler() {
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        this.typeReference = new TypeReference<T>() {
            @Override
            public Type getType() {
                return TypeReference(0);
            }
        };
    }

    /* 获取泛型的实际类型 index 表示第几个泛型参数 */
    private Type TypeReference(int index) {
        Type _type = null;
        Type superClass = this.getClass().getGenericSuperclass();
        if (superClass instanceof Class) {
            throw new IllegalArgumentException("Internal error: TypeReference constructed without actual type information");
        } else {
            _type = ((ParameterizedType) superClass).getActualTypeArguments()[index];
        }
        return _type;
    }

    /**
     * 设置非空参数
     *
     * @param ps             PreparedStatement
     * @param parameterIndex 参数
     * @param parameter      参数数组
     * @param jdbcType       jdbcType
     * @throws SQLException
     */
    @Override
    public void setNonNullParameter(PreparedStatement ps, int parameterIndex, T parameter, JdbcType jdbcType)
            throws SQLException {
        try {
            String jsonText = this.objectMapper.writeValueAsString(parameter);
            ps.setObject(parameterIndex, jsonText);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * 获取可以为null的结果集
     *
     * @param resultSet  结果集
     * @param columnName 列名
     */
    @Override
    public T getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        String jsonText = resultSet.getString(columnName);
        return readToObject(jsonText);
    }

    /**
     * 获取可以为null的结果集
     *
     * @param resultSet   结果集
     * @param columnIndex columnIndex 列标
     */
    @Override
    public T getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        String jsonText = resultSet.getString(columnIndex);
        return readToObject(jsonText);
    }

    /**
     * 获取可以为null的结果集
     *
     * @param callableStatement CallableStatement
     * @param columnIndex       列号
     */
    @Override
    public T getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        String jsonText = callableStatement.getString(columnIndex);
        return readToObject(jsonText);
    }

    private T readToObject(String jsonText) {
        if (StringUtils.isEmpty(jsonText)) {
            return null;
        }

        try {
            T list = this.objectMapper.readValue(jsonText, typeReference);
            return list;
        } catch (IOException e) {
            logger.error("字符串反序列化失败字符串为{}", jsonText, e);
        }

        return null;
    }
}
