package com.mybatis.config.handler.jts;

import com.mybatis.jts.converter.GenericGeometryConverter;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.locationtech.jts.geom.Geometry;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(Geometry.class)
@MappedJdbcTypes(JdbcType.OTHER)
public class JtsGeometryTypeHandler extends BaseTypeHandler<Geometry> {

    static final GenericGeometryConverter GEOMETRY_CONVERTER = new GenericGeometryConverter();

    public JtsGeometryTypeHandler() {

    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Geometry parameter, JdbcType jdbcType) throws SQLException {
        ps.setObject(i, GEOMETRY_CONVERTER.to(parameter, ps.getConnection().getMetaData().getDatabaseProductName()));
    }

    @Override
    public Geometry getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return GEOMETRY_CONVERTER.from(rs.getObject(columnName), rs.getStatement().getConnection().getMetaData().getDatabaseProductName());
    }

    @Override
    public Geometry getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return GEOMETRY_CONVERTER.from(rs.getObject(columnIndex), rs.getStatement().getConnection().getMetaData().getDatabaseProductName());
    }

    @Override
    public Geometry getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return GEOMETRY_CONVERTER.from(cs.getObject(columnIndex), cs.getConnection().getMetaData().getDatabaseProductName());
    }
}
