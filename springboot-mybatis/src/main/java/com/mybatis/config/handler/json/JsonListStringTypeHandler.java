package com.mybatis.config.handler.json;

import org.apache.ibatis.type.MappedTypes;

import java.util.List;

@MappedTypes({Object.class})
public class JsonListStringTypeHandler extends AbstractMysqlJsonTypeHandler<List<String>> {

}
