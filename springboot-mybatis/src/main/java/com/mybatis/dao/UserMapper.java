package com.mybatis.dao;

import com.mybatis.entity.User;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(String user);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> selectUser();
}