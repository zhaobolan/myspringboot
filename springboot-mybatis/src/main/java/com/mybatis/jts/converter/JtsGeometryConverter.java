package com.mybatis.jts.converter;

import com.mybatis.jts.jts.JtsGeometry;
import org.locationtech.jts.geom.Geometry;

import java.sql.SQLException;

/**
 * Converter JTS Geometry to/from byte array.
 * Inspired by
 * <a
 * href="http://www.dev-garden.org/2011/11/27/loading-mysql-spatial-data-with-jdbc-and-jts-wkbreader/">Loading
 * MySQL Spatial Data with JDBC and JTS WKBReader</a>. Using Object instead
 * of byte[] because of codegen.
 *
 * @ClassName GeometryConverter
 * @Date 2022-04-15 0:32
 * @Version 1.0
 */
public class JtsGeometryConverter extends GeometryConverter {

    /**
     * Convert byte array containing SRID + WKB Geometry into Geometry object
     */
    @Override
    public Geometry from(Object databaseObject) throws SQLException {
        final JtsGeometry jtsGeometry = (JtsGeometry) databaseObject;
        if (jtsGeometry == null) {
            return null;
        }
        // Read Geometry
        return jtsGeometry.getGeometry();
    }

    /**
     * Convert Geometry object into byte array containing SRID + WKB Geometry
     */
    @Override
    public Object to(Geometry geometry) throws SQLException {
        if (geometry == null) {
            return null;
        }

        JtsGeometry jtsGeometry = new JtsGeometry();
        jtsGeometry.setValue(geometry.toText()); //获取空间WKT字符串转JtsGeometry.claas 包装写入数据库
        return jtsGeometry;
    }

    @Override
    public Class fromType() {
        return JtsGeometry.class;
    }

    @Override
    public Class<Geometry> toType() {
        return Geometry.class;
    }

    @Override
    public String getDatabaseProductName() {
        return "PostgreSQL";
    }
}
