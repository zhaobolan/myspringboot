package com.mybatis.jts.geojson.document;

import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.introspection.IntrospectionDocumentFactory;

/**
 * Implementing types can create {@link Document documents} from an annotated object.
 * <p>
 * The factory is interchangeable to enable mocking in tests and customizations.
 * The default implementation can replaced with annotation attribute {@link GeoJsonType#factory()}.
 *
 * @see IntrospectionDocumentFactory as default implementation
 */
public interface DocumentFactory {

   /**
    * Creates a <em>GeoJSON document</em> from {@link GeoJsonType} annotation and the complementary annotations.
    *
    * @param object the object to introspect
    * @return one of {@link FeatureDocument}, {@link FeatureCollectionDocument}, {@link GeometryCollectionDocument} depending on the attribute {@link GeoJsonType#type()}
    * @throws DocumentFactoryException for missing annotations, wrong types, invalid combination of annotations and such
    */
   Document from(Object object) throws DocumentFactoryException;
}
