package com.mybatis.jts.geojson.collection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mybatis.jts.geojson.annotation.GeoJsonGeometry;
import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.feature.FeatureType;
import com.mybatis.jts.geojson.serializer.GeoJsonSerializer;
import org.locationtech.jts.geom.Geometry;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 *
 * @ClassName Feature
 * @Date 2022-03-21 9:05
 * @Version 1.0
 */
@GeoJsonType(type = FeatureType.FEATURE)
@JsonSerialize(using = GeoJsonSerializer.class)
@JsonDeserialize(using = GeoJsonDeserializer.class)
public class Feature<T extends Geometry> {

    public Feature() {
    }

    public Feature(T geom) {
        this.geom = geom;
    }

    private T geom;

    @GeoJsonGeometry
    public T getGeom() {
        return geom;
    }

    public void setGeom(T geom) {
        this.geom = geom;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "geom=" + geom +
                '}';
    }
}
