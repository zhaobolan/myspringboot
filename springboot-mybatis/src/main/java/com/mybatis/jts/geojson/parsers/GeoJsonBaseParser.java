package com.mybatis.jts.geojson.parsers;

import com.fasterxml.jackson.core.JsonParser;

import java.io.IOException;

public interface GeoJsonBaseParser<T> {

    T deserialize(JsonParser jsonParser) throws IOException;
}
