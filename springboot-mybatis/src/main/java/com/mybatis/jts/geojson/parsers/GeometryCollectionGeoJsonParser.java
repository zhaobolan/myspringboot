package com.mybatis.jts.geojson.parsers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ListMultimap;
import com.mybatis.jts.geojson.annotation.GeoJsonGeometries;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.document.DocumentFactoryException;
import com.mybatis.jts.geojson.introspection.Annotated;
import com.mybatis.jts.geojson.introspection.IntrospectionDocumentFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

import static com.mybatis.jts.jackson.GeomConstant.GEOMETRIES;

/**
 * 〈功能简述〉<br>
 * 〈GeometryCollectionGeoJsonParser〉
 *
 *
 * @ClassName GeometryCollectionGeoJsonParser
 * @Date 2022-03-14 9:37
 * @Version 1.0
 */
public class GeometryCollectionGeoJsonParser<T> extends BaseIgnoreDeserializerObjectMapper<T> implements GeoJsonBaseParser<T> {
    public GeometryCollectionGeoJsonParser(JavaType valueType, Class<? extends GeoJsonDeserializer> deserializeClazz) {
        super(valueType, deserializeClazz);
    }

    @Override
    public T deserialize(JsonParser jsonParser) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        //ObjectCodec oc = jsonParser.getCodec();
        ObjectNode nodeObject = (ObjectNode) mapper.readTree(jsonParser);
        // 取JSON数组
        ArrayNode geometries = nodeObject.withArray(GEOMETRIES);
        // 创建ObjectNode
        ObjectMapper copyMapper = getIgnoreDeserializerAnnotationMapper(mapper);
        ObjectNode objectNode = copyMapper.createObjectNode();
        //获取对象空间数据字段反序列化名称
        String geometriesFieldName = null;
        try {
            ListMultimap<Class<? extends Annotation>, Annotated> index = IntrospectionDocumentFactory.index(valueType.getRawClass());
            Annotated featuresAnnotated = IntrospectionDocumentFactory.oneOrNull(index, GeoJsonGeometries.class);
            geometriesFieldName = featuresAnnotated.getName();
        } catch (DocumentFactoryException e) {
            e.printStackTrace();
        }
        objectNode.set(geometriesFieldName, geometries);
        //JsonParser parser = copyMapper.treeAsTokens(objectNode);
        //T t = copyMapper.readValue(parser, valueType);
        T t = copyMapper.convertValue(objectNode, valueType);
        return t;

    }
}