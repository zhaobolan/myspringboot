package com.mybatis.jts.geojson.parsers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ListMultimap;
import com.mybatis.jts.geojson.annotation.GeoJsonGeometry;
import com.mybatis.jts.geojson.annotation.GeoJsonId;
import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.document.DocumentFactoryException;
import com.mybatis.jts.geojson.introspection.Annotated;
import com.mybatis.jts.geojson.introspection.IntrospectionDocumentFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

import static com.mybatis.jts.jackson.GeomConstant.*;

/**
 * 〈功能简述〉<br>
 * 〈FeatureGeoJsonParser〉
 *
 *
 * @ClassName FeatureGeoJsonParser
 * @Date 2022-03-14 9:36
 * @Version 1.0
 */
public class FeatureGeoJsonParser<T> extends BaseIgnoreDeserializerObjectMapper<T> implements GeoJsonBaseParser<T> {

    public FeatureGeoJsonParser(JavaType valueType, Class<? extends GeoJsonDeserializer> deserializeClazz) {
        super(valueType, deserializeClazz);
    }

    @Override
    public T deserialize(JsonParser jsonParser) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        //ObjectCodec oc = jsonParser.getCodec();
        ObjectNode nodeObject = (ObjectNode) mapper.readTree(jsonParser);
        GeoJsonType geoJsonType = valueType.getRawClass().getAnnotation(GeoJsonType.class);
        String featureName = geoJsonType.type().getName();

        //获取对象空间数据字段反序列化名称
        String geometryFieldName = null;
        String idFieldName = null;
        try {
            ListMultimap<Class<? extends Annotation>, Annotated> index = IntrospectionDocumentFactory.index(valueType.getRawClass());
            Annotated geometryAnnotated = IntrospectionDocumentFactory.oneOrNull(index, GeoJsonGeometry.class);
            Annotated idAnnotated = IntrospectionDocumentFactory.oneOrNull(index, GeoJsonId.class);
            if (null != geometryAnnotated) {
                geometryFieldName = geometryAnnotated.getName();
            }
            if (null != idAnnotated) {
                idFieldName = idAnnotated.getName();
            }
        } catch (DocumentFactoryException e) {
            e.printStackTrace();
        }
        //获取数据
        JsonNode id = nodeObject.get(ID);
        JsonNode geometry = nodeObject.get(GEOMETRY);
        ObjectNode properties = (ObjectNode) nodeObject.get(PROPERTIES);
        //组装
        nodeObject.remove(ID);
        nodeObject.remove(TYPE);
        nodeObject.remove(GEOMETRY);
        nodeObject.remove(PROPERTIES);
        if (null != idFieldName && null != id) {
            nodeObject.replace(idFieldName, id);
        }
        if (null != geometryFieldName && null != geometry) {
            nodeObject.replace(geometryFieldName, geometry);
        }
        if (null != properties) {
            nodeObject.setAll(properties);
        }
        ObjectMapper copyMapper = getIgnoreDeserializerAnnotationMapper(mapper);
//    JsonParser parser = copyMapper.treeAsTokens(nodeObject);
//    T t = copyMapper.readValue(parser, valueType);
        T t = copyMapper.convertValue(nodeObject, valueType);
        return t;
    }
}