package com.mybatis.jts.geojson.collection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mybatis.jts.geojson.annotation.GeoJsonBbox;
import com.mybatis.jts.geojson.annotation.GeoJsonFeatures;
import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.feature.FeatureType;
import com.mybatis.jts.geojson.serializer.GeoJsonSerializer;

import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 *
 * @ClassName FeatureCollection
 * @Date 2022-03-08 17:54
 * @Version 1.0
 */
@GeoJsonType(type = FeatureType.FEATURE_COLLECTION)
@JsonSerialize(using = GeoJsonSerializer.class)
@JsonDeserialize(using = GeoJsonDeserializer.class)
public class FeatureCollection<T> {

   private List<T> list;

   private double[] bbox;

   @GeoJsonFeatures
   public List<T> getList() {
      return list;
   }

   public void setList(List<T> list) {
      this.list = list;
   }

   @GeoJsonBbox
   public double[] getBbox() {
      return bbox;
   }

   public void setBbox(double[] bbox) {
      this.bbox = bbox;
   }

   public FeatureCollection<T> build(List<T> list){
      this.list = list;
      return this;
   }
}
