package com.mybatis.jts.geojson.parsers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ListMultimap;
import com.mybatis.jts.geojson.annotation.GeoJsonFeatures;
import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.document.DocumentFactoryException;
import com.mybatis.jts.geojson.introspection.Annotated;
import com.mybatis.jts.geojson.introspection.IntrospectionDocumentFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

import static com.mybatis.jts.jackson.GeomConstant.*;

/**
 * 〈功能简述〉<br>
 * 〈FeatureCollectionGeoJsonParser〉
 *
 *
 * @ClassName FeatureCollectionGeoJsonParser
 * @Date 2022-03-14 9:35
 * @Version 1.0
 */
public class FeatureCollectionGeoJsonParser<T> extends BaseIgnoreDeserializerObjectMapper<T> implements GeoJsonBaseParser<T> {

    public FeatureCollectionGeoJsonParser(JavaType valueType, Class<? extends GeoJsonDeserializer> deserializeClazz) {
        super(valueType, deserializeClazz);
    }

    @Override
    public T deserialize(JsonParser jsonParser) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jsonParser.getCodec();
        //ObjectCodec oc = jsonParser.getCodec();
        GeoJsonType geoJsonType = valueType.getRawClass().getAnnotation(GeoJsonType.class);
        ObjectNode nodeObject = (ObjectNode) mapper.readTree(jsonParser);
        // 取JSON数组
        ArrayNode features = nodeObject.withArray(FEATURES);
        JsonNode bbox = nodeObject.get(BBOX);
        // 创建ObjectNode
        ObjectMapper copyMapper = getIgnoreDeserializerAnnotationMapper(mapper);
        ObjectNode objectNode = copyMapper.createObjectNode();
        //获取对象空间数据字段反序列化名称
        String featuresFieldName = null;
        try {
            ListMultimap<Class<? extends Annotation>, Annotated> index = IntrospectionDocumentFactory.index(valueType.getRawClass());
            Annotated featuresAnnotated = IntrospectionDocumentFactory.oneOrNull(index, GeoJsonFeatures.class);
            featuresFieldName = featuresAnnotated.getName();
        } catch (DocumentFactoryException e) {
            e.printStackTrace();
        }
        if (null != featuresFieldName && null != features) {
            objectNode.replace(featuresFieldName, features);
        }
        if (null != bbox) {
            objectNode.replace(BBOX, bbox);
        }
        //JsonParser parser = copyMapper.treeAsTokens(objectNode);
        //T t = copyMapper.readValue(parser, valueType);
        T t = copyMapper.convertValue(objectNode, valueType);
        return t;
    }
}
