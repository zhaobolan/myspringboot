package com.mybatis.jts.geojson.collection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mybatis.jts.geojson.annotation.GeoJsonGeometries;
import com.mybatis.jts.geojson.annotation.GeoJsonType;
import com.mybatis.jts.geojson.deserializer.GeoJsonDeserializer;
import com.mybatis.jts.geojson.feature.FeatureType;
import com.mybatis.jts.geojson.serializer.GeoJsonSerializer;
import org.locationtech.jts.geom.Geometry;

import java.util.List;

/**
 * 〈功能简述〉<br>
 * 〈〉
 *
 *
 * @ClassName GeometryCollection
 * @Date 2022-03-08 17:56
 * @Version 1.0
 */
@GeoJsonType(type = FeatureType.GEOMETRY_COLLECTION)
@JsonSerialize(using = GeoJsonSerializer.class)
@JsonDeserialize(using = GeoJsonDeserializer.class)
public class GeometryCollection<T extends Geometry> {

   private List<T> list;

   @GeoJsonGeometries
   public List<T> getList() {
      return list;
   }

   public void setList(List<T> list) {
      this.list = list;
   }
   public GeometryCollection<T> build(List<T> list){
      this.list = list;
      return this;
   }
}
