package com.mybatis.jts.geojson.crs;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CrsType {
    @JsonProperty("name")
    name,
    @JsonProperty("link")
    link;
}
