package com.mybatis.controller;


import javax.annotation.Resource;

import com.github.pagehelper.PageInfo;
import com.mybatis.entity.User;
import com.mybatis.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping(value = "{id}")
    public User getUser(@PathVariable long id) throws Exception {
        return this.userService.getUserById(id);
    }

    @GetMapping(value="listUser")
    public PageInfo<User> listUser(
            @RequestParam(value="page", required=false, defaultValue="1") int page,
            @RequestParam(value="page-size", required=false, defaultValue="5") int pageSize){
        List<User> result = userService.listUser(page, pageSize);
        // PageInfo包装结果，返回更多分页相关信息
        PageInfo<User> pi = new PageInfo<User>(result);
        return pi;
    }

    @PostMapping(value="/saveUser")
    public User saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }

    @PostMapping(value="/updateUser")
    public User updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @GetMapping(value="/batchSaveUser")
    public void batchSaveUser(){
         userService.batchSaveUser();
    }

    @GetMapping(value="/testTransactionalTimeOut")
    public void testTransactionalTimeOut(){
        userService.testTransactionalTimeOut();
    }
}
