package com.mybatis.service.impl;

import cn.hutool.core.util.IdUtil;
import com.github.pagehelper.PageHelper;
import com.mybatis.dao.UserMapper;
import com.mybatis.entity.User;
import com.mybatis.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;


@Service("userService")
public class UserServiceImpl implements UserService {

    // 注入mapper类
    @Resource
    private UserMapper userMapper;

    @Override
    public User getUserById(long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Override
    public List<User> listUser(int page, int pageSize) {
        List<User> result = null;
        try {
            // 调用pagehelper分页，采用starPage方式。starPage应放在Mapper查询函数之前
            PageHelper.startPage(page, pageSize); //每页的大小为pageSize，查询第page页的结果
            PageHelper.orderBy("id ASC "); //进行分页结果的排序
            result = userMapper.selectUser();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public User saveUser(User user) {
        user.setId(IdUtil.getSnowflakeNextId());
        userMapper.insert(user);
        return user;
    }

    @Override
    public User updateUser(User user) {
        userMapper.updateByPrimaryKeySelective(user);
        return user;
    }

    @Override
    public void batchSaveUser() {
        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setId(IdUtil.getSnowflakeNextId());
            user.setNickname("user" + i);
            try {
                if(i==3 || i == 5) {
                    throw new RuntimeException();
                }
                saveToUser(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public User saveToUser(User user) {
        user.setId(IdUtil.getSnowflakeNextId());
        userMapper.insert(user);
        update(user);
        return user;
    }

    private void update(User user) {
        user.setMobile("123456");
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    @Transactional(rollbackFor = Exception.class,timeout = 2)
    public void testTransactionalTimeOut() {
        System.out.println("startTime:"+ LocalDateTime.now());
        User user = new User();
        user.setId(IdUtil.getSnowflakeNextId());
        user.setNickname("user");
        userMapper.insert(user);

        try {
            TimeUnit.SECONDS.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        user.setMobile("123456");
        userMapper.updateByPrimaryKeySelective(user);
        System.out.println("endTime:"+ LocalDateTime.now());
    }
}
