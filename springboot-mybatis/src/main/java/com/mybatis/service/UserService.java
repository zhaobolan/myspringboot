package com.mybatis.service;


import com.mybatis.entity.User;

import java.util.List;

public interface UserService {
    User getUserById(long userId);

    List<User> listUser(int page, int pageSize);

    User saveUser(User user);

    User updateUser(User user);

    void batchSaveUser();

    void testTransactionalTimeOut();
}
