package com.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@MapperScan("com.mybatis.dao")
public class SpringbootMybatisApplication {

    private static Logger log = LoggerFactory.getLogger(SpringbootMybatisApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(SpringbootMybatisApplication.class, args);
        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 'mybatis整合案例' 启动成功! 访问连接:\n\t" +
                        "任务调度中心: \t\thttp://{}:{}\n\t" +
                        "----------------------------------------------------------",
                "127.0.0.1",
                env.getProperty("server.port")
        );
    }

}
