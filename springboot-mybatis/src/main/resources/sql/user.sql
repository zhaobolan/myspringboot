CREATE TABLE `user` (
    `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'id',
    `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
    `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
    `password` char(60) DEFAULT NULL COMMENT '密码hash值',
    `role` varchar(100) DEFAULT 'user' COMMENT '角色，角色名以逗号分隔',
    `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    `details` json DEFAULT NULL COMMENT '用户详情',
    `geom` geometry DEFAULT NULL COMMENT '用户坐标',
    PRIMARY KEY (`id`)
)COMMENT='用户表';

INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`)VALUES ('abc1', '13512345678',  '123','2023-04-06 10:04:41' );
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`)VALUES ('abc2', '13512345677',  '123','2023-04-06 10:04:42' );
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc3', '13512345603', '123','2023-04-06 10:04:43');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc4', '13512345604', '123','2023-04-06 10:04:44');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc5', '13512345605', '123','2023-04-06 10:04:45');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc6', '13512345606', '123','2023-04-06 10:04:46');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc7', '13512345607', '123','2023-04-06 10:04:47');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc8', '13512345608', '123','2023-04-06 10:04:48');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc9', '13512345609', '123','2023-04-06 10:04:49');
INSERT INTO `user` (`nickname`, `mobile`, `password`,`create_time`) VALUES ('abc10', '13512345610','123','2023-04-06 10:04:50');



