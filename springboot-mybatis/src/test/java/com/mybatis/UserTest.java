package com.mybatis;

import com.mybatis.dao.UserMapper;
import com.mybatis.entity.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
public class UserTest {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;


    /**
     * 一级缓存演示
     */
    @Test
    public void test() {
        // 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 获取UserMapper接口的代理对象
        UserMapper userMapper1 = sqlSession.getMapper(UserMapper.class);

        // 执行sql
        User user1 = userMapper1.selectByPrimaryKey(2L);
        System.out.println(user1);
        System.out.println("\n------------------------------------\n");

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 此处更新之后，则会清除缓存(只要存在更新或删除操作都会进行清除，无论是否是当前数据，因此很容易失效)

        User user = new User();
        user.setId(3L);
        user.setNickname("33");
        userMapper1.updateByPrimaryKeySelective(user);

        User user2 = userMapper1.selectByPrimaryKey(2L);
        System.out.println(user2);
        // 关闭资源
        sqlSession.close();
    }

    /**
     * 二级缓存演示
     */
    @Test
    void test2() {
        // 获取SqlSession对象，用它来执行sql
        SqlSession sqlSession1 = sqlSessionFactory.openSession();


        // 执行sql
        // 获取UserMapper接口的代理对象
        UserMapper userMapper1 = sqlSession1.getMapper(UserMapper.class);
        User user1 = userMapper1.selectByPrimaryKey(2L);
        System.out.println(user1);
        // 关闭资源
        sqlSession1.close();


        System.out.println("\n------------------------------------\n");


        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        UserMapper userMapper2 = sqlSession2.getMapper(UserMapper.class);
        User user2 = userMapper2.selectByPrimaryKey(2L);
        System.out.println(user2);

        // 关闭资源
        sqlSession2.close();
    }

}
