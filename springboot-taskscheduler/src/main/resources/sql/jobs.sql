/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.211.131
 Source Server Type    : MySQL
 Source Server Version : 80100
 Source Host           : 192.168.211.131:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80100
 File Encoding         : 65001

 Date: 03/06/2024 17:53:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint NOT NULL COMMENT '主键ID',
  `task_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'bean名称',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '任务名称',
  `cron` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'Cron表达式',
  `status` int NOT NULL COMMENT '状态:0-禁用，1-启用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '周期任务配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES (1, 'job1', '测试定时任务JOB1', '0/3 * * * * ? ', 1, '每30秒执行一次', '2024-06-03 16:57:56', NULL);
INSERT INTO `jobs` VALUES (2, 'job2', '测试定时任务JOB2', '0 0/1 * * * ? ', 0, '每一分钟执行一次', '2024-06-03 17:00:37', NULL);

SET FOREIGN_KEY_CHECKS = 1;
