package com.taskscheduler.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Random;

public class JsonUtil {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 将集合转换为JSON字符串
     * @param collection 要转换的集合
     * @param <T> 集合中元素的类型
     * @return JSON字符串
     */
    public static <T> String serializeListToJson(List<T> collection) {
        try {
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            return objectMapper.writeValueAsString(collection);
        } catch (IOException e) {
            // 处理异常，实际应用中应该有更详细的错误处理逻辑
            throw new RuntimeException("Failed to serialize list to JSON", e);
        }
    }


    /**
     * 生成指定范围内的随机数。
     *
     * @param min 区间最小值（包含）
     * @param max 区间最大值（包含）
     * @return 范围[min, max]内的随机整数
     */
    public static int getRandomInRange(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("最小值不能大于最大值");
        }

        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}