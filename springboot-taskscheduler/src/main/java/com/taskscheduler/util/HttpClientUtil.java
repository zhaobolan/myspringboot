package com.taskscheduler.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class HttpClientUtil {

    /**
     * 发送GET请求
     *
     * @param url 请求的URL
     * @return 响应内容
     */
    public String doGet(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        try (CloseableHttpResponse response = httpClient.execute(httpGet)) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, "UTF-8");
            }
        } finally {
            httpClient.close();
        }
        return null;
    }

    /**
     * 发送POST请求
     *
     * @param url       请求的URL
     * @param jsonBody 请求体（JSON格式）
     * @return 响应内容
     */
    public String doPost(String url, String jsonBody, Map<String, Object> headers) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-Type", "application/json;charset=UTF-8");
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            httpPost.setHeader(entry.getKey(), entry.getValue().toString());
        }
        StringEntity entity = new StringEntity(jsonBody, "UTF-8");
        httpPost.setEntity(entity);

        try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                return EntityUtils.toString(responseEntity, "UTF-8");
            }
        } finally {
            httpClient.close();
        }
        return null;
    }
}