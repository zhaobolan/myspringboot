package com.taskscheduler.po;

import lombok.Data;

import java.util.Date;

@Data
public class Jobs {
    private Long id;

    private String taskKey;

    private String name;

    private String cron;

    private Integer status;

    private String remark;

    private Date createTime;

    private Date updateTime;
}