package com.taskscheduler;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.taskscheduler.mapper")
public class SpringbootTaskSchedulerApplication{

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTaskSchedulerApplication .class, args);
    }
}