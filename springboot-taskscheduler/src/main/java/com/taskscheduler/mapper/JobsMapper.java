package com.taskscheduler.mapper;

import com.taskscheduler.po.Jobs;

import java.util.List;

public interface JobsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Jobs record);

    int insertSelective(Jobs record);

    Jobs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Jobs record);

    int updateByPrimaryKey(Jobs record);

    List<Jobs> listAll();
}