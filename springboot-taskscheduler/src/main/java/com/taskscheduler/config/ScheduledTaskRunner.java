package com.taskscheduler.config;

import com.taskscheduler.service.ScheduledTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 该类用于启动时,开启需要自启的任务
 */
@Slf4j
@Component
public class ScheduledTaskRunner implements ApplicationRunner {

    @Resource
    private ScheduledTaskService scheduledTaskService;

    @Override
    public void run(ApplicationArguments args) {
        log.info(" >>>>>> 项目启动完毕, 开启 => 需要自启的任务 开始!");
        scheduledTaskService.startAllTask();
        log.info(" >>>>>> 项目启动完毕, 开启 => 需要自启的任务 结束！");
    }
}