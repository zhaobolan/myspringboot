package com.taskscheduler.job;

/**
 * 所有任务父类(所有任务必须实现该接口)
 */
public interface ScheduledTaskJob extends Runnable {

}
