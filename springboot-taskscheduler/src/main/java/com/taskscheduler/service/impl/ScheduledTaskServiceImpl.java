package com.taskscheduler.service.impl;

import com.taskscheduler.job.ScheduledTaskJob;
import com.taskscheduler.mapper.JobsMapper;
import com.taskscheduler.po.Jobs;
import com.taskscheduler.service.ScheduledTaskService;
import com.taskscheduler.util.SpringApplicationContextProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;


@Slf4j
@Service
public class ScheduledTaskServiceImpl implements ScheduledTaskService {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    @Resource
    private ApplicationContext applicationContext;
    @Resource
    private JobsMapper jobsMapper;

    /**
     * 记录所有已经启动的任务
     */
    private final Map<String, ScheduledFuture<?>> scheduledFutureMap = new ConcurrentHashMap<>();

    @Override
    public void startAllTask() {
        // 获取所有启用的任务
        List<Jobs> jobs = jobsMapper.listAll();

        // 启动所有任务
        for (Jobs job : jobs) {
            String taskKey = job.getTaskKey();
            ScheduledTaskJob scheduledTaskJob = (ScheduledTaskJob) applicationContext.getBean(taskKey);
            log.info(">>>>>>>>> 任务 [ {} ] ,cron={}", job.getName(), job.getCron());
            ScheduledFuture<?> scheduledFuture = threadPoolTaskScheduler.schedule(
                    scheduledTaskJob, i -> new CronTrigger(job.getCron()).nextExecutionTime(i));
            log.info("任务:{} >>>>>>>>>>>>>>>>>启动完成<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<", job.getName());
            scheduledFutureMap.put(taskKey, scheduledFuture);
        }
    }

    @Override
    public void stopAllTask() {
        // 获取所有启用的任务
        List<Jobs> jobs = jobsMapper.listAll();

        for (Jobs job : jobs) {
            String taskKey = job.getTaskKey();
            ScheduledFuture<?> scheduledFuture = scheduledFutureMap.get(taskKey);
            scheduledFuture.cancel(true);
            scheduledFutureMap.remove(taskKey);
            log.info("任务:{} >>>>>>>>>>>>>>>>>停止完成<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<", job.getName());
        }
    }

    @Override
    public void restartTask(Long id) {
        // 查询任务信息
        Jobs job = jobsMapper.selectByPrimaryKey(id);

        // 停止任务
        String taskKey = job.getTaskKey();
        ScheduledFuture<?> scheduledFuture = scheduledFutureMap.get(taskKey);
        scheduledFuture.cancel(true);
        scheduledFutureMap.remove(taskKey);

        // 启动任务
        ScheduledTaskJob scheduledTaskJob = (ScheduledTaskJob) SpringApplicationContextProvider.getBean(taskKey);
        log.info(">>>>>>>>> 任务 [ {} ] ,cron={}", job.getName(), job.getCron());
        ScheduledFuture<?> scheduledFuture1 = threadPoolTaskScheduler.schedule(
                scheduledTaskJob, i -> new CronTrigger(job.getCron()).nextExecutionTime(i));
        log.info("任务:{} >>>>>>>>>>>>>>>>>重启完成<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<", job.getName());
        scheduledFutureMap.put(taskKey, scheduledFuture1);
    }
}
