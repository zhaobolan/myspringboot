package com.taskscheduler.service;

import org.springframework.stereotype.Service;

@Service
public interface ScheduledTaskService {

    /**
     * 启动所有定时任务
     *
     * @date 2024/6/3 16:11
     */
    void startAllTask();

    /**
     * 停止所有定时任务
     *
     * @date 2024/6/3 16:11
     */
    void stopAllTask();

    /**
     * 重启指定任务
     *
     * @date 2024/6/3 16:11
     */
    void restartTask(Long id);
}
