package com.taskscheduler.controller;

import com.taskscheduler.service.ScheduledTaskService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class JobTaskController {

    @Resource
    private ScheduledTaskService scheduledTaskService;

    /**
     * 启动所有任务
     */
    @GetMapping("/startAllJob")
    public void startAllJob() {
        scheduledTaskService.startAllTask();
    }

    /**
     * 停止所有任务
     */
    @GetMapping("/stopAllJob")
    public void stopAllJob() {
        scheduledTaskService.stopAllTask();
    }

    /**
     * 重启指定任务
     */
    @GetMapping("/restartJob/{id}")
    public void restartJob(@PathVariable("id") Long id) {
        scheduledTaskService.restartTask(id);
    }
}
