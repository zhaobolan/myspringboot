package com.taskscheduler.task;

import com.taskscheduler.job.ScheduledTaskJob;
import com.taskscheduler.util.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.taskscheduler.util.JsonUtil.getRandomInRange;

/**
 * 测试定时任务1
 *
 * @author wyj
 * @date 2024/6/3 16:55
 */
@Slf4j
@Component
public class Job2 implements ScheduledTaskJob {

    @Resource
    private HttpClientUtil httpClientUtil;

    private AtomicInteger count = new AtomicInteger(0);

    private static final String BASE = "{\n" +
            "    \"data\": {\n" +
            "        \"deviceData\": {\n" +
            "            \"electricPercentage\": 20,\n" +
            "            \"nBSignal\": {\n" +
            "                \"rsrp\": -95\n" +
            "            },\n" +
            "            \"pressure\": ${pressure},\n" +
            "            \"temperature\": ${temperature},\n" +
            "            \"statusUpdateTime\": ${statusUpdateTime}\n" +
            "        }\n" +
            "    },\n" +
            "    \"dataType\": 2,\n" +
            "    \"archiveId\": \"${archiveId}\"\n" +
            "}";
    @Override
    public void run() {
        log.info("场站上报数据====start");
//        int andIncrement = count.getAndIncrement();
//        // 创建LocalDate实例表示2024年6月1日
//        Long startTime = 1718347620000L;
//        long l = startTime + 1 * 60 * 1000 * andIncrement;
//        // 如果上报时间大约等于2024年6月14日凌晨，则停止上报
//        if (l > 1718349300000L) {
//            System.out.println("==========================================end==========================================");
//            System.exit(0);
//        }

        try {
            String url = "https://gwc.cdqckj.com/api/gis-station/gasDeviceMonitor/saveData";
            String param = BASE.replace("${archiveId}", "4f1f6a3c102442e5a48122867c09e653")
                    .replace("${pressure}", String.valueOf(getRandomInRange(281, 310)))
                    .replace("${temperature}", String.valueOf(getRandomInRange(20, 25)))
//                    .replace("${statusUpdateTime}", String.valueOf(l))
                    .replace("${statusUpdateTime}", String.valueOf(System.currentTimeMillis()))
                    ;
            Map<String, Object> headers = new HashMap<>();
            headers.put("tenant", "c2ljaHVhbmJucnFncw==");
            headers.put("pt", "zugmis");
            headers.put("Authorization", "Basic Z21pc191aTpnbWlzX3VpX3NlY3JldA==");
            String result = httpClientUtil.doPost(url, param, headers);
            log.info("上报数据响应结果:{}", result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("场站上报数据====end");
    }
}
