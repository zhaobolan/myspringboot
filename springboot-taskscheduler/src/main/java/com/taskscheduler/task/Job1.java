package com.taskscheduler.task;

import com.taskscheduler.job.ScheduledTaskJob;
import com.taskscheduler.util.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.taskscheduler.util.JsonUtil.getRandomInRange;


/**
 * 测试定时任务1
 *
 * @author wyj
 * @date 2024/6/3 16:55
 */
@Slf4j
@Component
public class Job1 implements ScheduledTaskJob {

    @Resource
    private HttpClientUtil httpClientUtil;

    private static final String BASE = "{\n" +
            "    \"data\": {\n" +
            "        \"deviceData\": {\n" +
            "            \"electricPercentage\": 20,\n" +
            "            \"nBSignal\": {\n" +
            "                \"rsrp\": -95\n" +
            "            },\n" +
            "            \"pressure\": ${pressure},\n" +
            "            \"temperature\": ${temperature},\n" +
            "            \"statusUpdateTime\": ${statusUpdateTime}\n" +
            "        }\n" +
            "    },\n" +
            "    \"dataType\": 2,\n" +
            "    \"archiveId\": \"${archiveId}\"\n" +
            "}";

    @Override
    public void run() {
        log.info("管网上报数据====start");
        try {
            String url = "https://gwc.cdqckj.com/api/gis-monitor/gasDeviceMonitor/saveData";
            List<String> device = Arrays.asList(
                    "a8dfe43be9c14ca6bf5f2a9d2c6d4f5a",
                    "3ebc23b78c6f42879a3bf4b0a2cdd4d3",
                    "5b9c1bfffae2470aa35a7dc47a2b15f6",
                    "d9e05a962f3b48648f2c2c5bdd10c8f"
            );
            for (String s : device) {
                String param = BASE.replace("${archiveId}", s)
                        .replace("${pressure}", String.valueOf(getRandomInRange(250, 299)))
                        .replace("${temperature}", String.valueOf(getRandomInRange(20, 25)))
                        .replace("${statusUpdateTime}", String.valueOf(System.currentTimeMillis()));
                Map<String, Object> headers = new HashMap<>();
                headers.put("tenant", "c2ljaHVhbmJucnFncw==");
                headers.put("pt", "zugmis");
                headers.put("Authorization", "Basic Z21pc191aTpnbWlzX3VpX3NlY3JldA==");
                String result = httpClientUtil.doPost(url, param, headers);
                log.info("上报数据响应结果:{}", result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("管网上报数据====end");
    }
}
