package com.minio;

import com.minio.service.MinioFileService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@SpringBootTest
@Slf4j
class SpringbootMinioApplicationTests {

    @Autowired
    private MinioFileService minioFileService;


    @Test
    public void reviewFile() {

    }


    @Test
    public void testDeleteFile() {
        String pathUrl = "http://192.168.211.131:9000/gmis/2024/6/17/Snipaste_2024-04-22_14-47-28.png";
        minioFileService.delete(pathUrl);
    }

    @Test
    public void testDownloadFile() {
        String pathUrl = "http://192.168.211.131:9000/gmis/2024/6/17/Snipaste_2024-04-22_14-47-28.png";
        byte[] bytes = minioFileService.getFile(pathUrl);
        File file = null;
        FileOutputStream fos = null;
        try {
            //将bytes数组转为文件
            file = new File("D:\\weiyiji.png");
            //根据文件构建文件输出流
            fos = new FileOutputStream(file);

            //写byte到文件输出流里
            fos.write(bytes, 0, bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //关闭流
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
