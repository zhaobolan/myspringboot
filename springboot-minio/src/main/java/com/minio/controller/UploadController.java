package com.minio.controller;


import cn.hutool.core.lang.Assert;
import cn.hutool.json.JSONObject;
import com.google.common.collect.ImmutableMap;
import com.minio.service.MinioChunkFileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 大文件分片上传
 * 上传步骤：
 *  1.前端人员将文件进行分片
 *  2.调用/multipart/init接口获取uploadId和所有分片的上传地址
 *  3.前端人员调用步骤2返回的地址进行分片上传
 *  4.调用/multipart/complete接口进行分片合并
 *
 *  提示：MinIO服务器控制台上面是看不到已经上传的分片的哦，只有已经合并了的文件才可以看到
 *
 *  MinIO其实本身对文件进行了区分的，会自动进行分片上传，因此此种方式感觉不太实用，因为分片上传，我们是需要自己记录分片的相关信息，才能做到断点续传
 *  这个只是一个演示MinIO的案列而已，如果要真正实用到项目中可以参考spingboot-file项目
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/largeFileUpload")
public class UploadController {

    @Resource
    private MinioChunkFileService minioChunkFileService;

//    1.用户调用初始化接口，后端调用minio初始化，得到uploadId，生成每个分片的minio上传url
//初始化 示例： $ curl --location --request POST 'http://127.0.0.1:9998/admin/largeFileUpload/multipart/init' \
//            --header 'Content-Type: application/json' \
//            --data-raw '{
    //            "filename": "result.png",
    //            "partCount": 2
//              }'
    //    返回示例：
//{
//    "uploadId": "eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3",
//        "uploadUrls": [
//    "http://ip:端口号/uav/20230913/result.png?uploadId=eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3&partNumber=1&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=lengleng%2F20230913%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230913T031427Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=9e7bd28d6f4d42d0ed447750cadcd8782ccc2648a66ecb25a1e7639593ad7dd6",
//            "http://ip:端口号/uav/20230913/result.png?uploadId=eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3&partNumber=2&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=lengleng%2F20230913%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230913T031427Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=60d31989b9dc3bdacf6bbc9c13cb5c982f79b1e3a0bbcb948e0ec90fede2f9f3"
//    ]
//}
//    2.用户调用对应分片的上传地址，多次上传会覆盖
//    示例：
//    curl --location --request PUT 'http://ip:端口号/uav/20230913/result.png?uploadId=eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3&partNumber=1&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=lengleng%2F20230913%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230913T031427Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=9e7bd28d6f4d42d0ed447750cadcd8782ccc2648a66ecb25a1e7639593ad7dd6' \
//            --header 'Content-Type: application/octet-stream' \
//            --data-binary '@/D:/jpgone'
//    curl --location --request PUT 'http://ip:端口号/uav/20230913/result.png?uploadId=eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3&partNumber=2&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=lengleng%2F20230913%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230913T031427Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=60d31989b9dc3bdacf6bbc9c13cb5c982f79b1e3a0bbcb948e0ec90fede2f9f3' \
//            --header 'Content-Type: application/octet-stream' \
//            --data-binary '@/D:/jpgtwo'
//    3.调用完成接口，后端查询所有上传的分片并合并，uploadId为第一步返回的uploadId
//    示例：
//    curl --location --request PUT '127.0.0.1:8006/multipart/complete' \
//            --header 'Content-Type: application/json' \
//            --data-raw '{
    //    "objectName":"20230913/result.png",
    //    "uploadId":"eb163e3d-bbf5-48ae-aa56-85cf2a3c0fa3"
//                      }'

    /**
     * 分片初始化
     *
     * @param requestParam 请求参数
     * @return /
     */
    @PostMapping("/multipart/init")
    public ResponseEntity<Object> initMultiPartUpload(@RequestBody JSONObject requestParam) {
        // 路径（存入minio的路径）
        String path = requestParam.getStr("path", "20240619");
        // 文件名
        String filename = requestParam.getStr("filename", "test.obj");
        // content-type
        String contentType = requestParam.getStr("contentType", "application/octet-stream");
        // md5-可进行秒传判断
        String md5 = requestParam.getStr("md5", "");
        // 分片数量
        Integer partCount = requestParam.getInt("partCount", 100);

        //TODO::业务判断+秒传判断

        Map<String, Object> result = minioChunkFileService.initMultiPartUpload(path, filename, partCount, contentType);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 合并分片文件，完成上传
     *
     * @param requestParam 用户参数
     * @return /
     */
    @PutMapping("/multipart/complete")
    public ResponseEntity<Object> completeMultiPartUpload(@RequestBody JSONObject requestParam) {
        // 文件名完整路径
        String objectName = requestParam.getStr("objectName");
        //初始化返回的uploadId
        String uploadId = requestParam.getStr("uploadId");
        Assert.notNull(objectName, "objectName must not be null");
        Assert.notNull(uploadId, "uploadId must not be null");
        boolean result = minioChunkFileService.mergeMultipartUpload(objectName, uploadId);
        return new ResponseEntity<>(ImmutableMap.of("success", result), HttpStatus.OK);
    }
}

