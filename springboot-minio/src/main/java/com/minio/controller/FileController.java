package com.minio.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import com.minio.service.MinioFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping
public class FileController {

    @Autowired
    private MinioFileService minioFileService;


    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file,
                         @RequestParam("targetFolder") String targetFolder,
                         @RequestParam("newFilename") String newFilename) {
        return minioFileService.uploadFile(file, targetFolder, newFilename);
    }

    // 文件预览
    @GetMapping("/preview")
    public String preview(@RequestParam("url") String url) {
        String s = HttpUtil.get("http://139.186.144.124:9096/gmis-test/test/Snipaste_2024-04-28_14-18-34.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=gmis%2F20240625%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20240625T032708Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=4980b0036aa0b96633f7b2a7c7300231028f4159ded5df7d1814682c2ff85023");

//        return minioFileService.previewFile(fileName);
        return null;
    }
}
