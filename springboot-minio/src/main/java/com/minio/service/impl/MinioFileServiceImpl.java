package com.minio.service.impl;


import cn.hutool.core.util.StrUtil;
import com.minio.config.CustomMinioClient;
import com.minio.config.MinioProperties;
import com.minio.service.MinioFileService;
import com.minio.util.ViewContentTypeEnum;
import io.minio.GetObjectArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.http.Method;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

@Service
@Slf4j
public class MinioFileServiceImpl implements MinioFileService {

    @Autowired
    private MinioProperties minioProperties;
    @Autowired
    private CustomMinioClient minioClient;

    @Override
    public String uploadFile(MultipartFile file, String targetFolder, String newFilename) {
        try (InputStream fis = file.getInputStream()) {
            String originalFilename = file.getOriginalFilename();
            String bucketName = minioProperties.getBucketName();

            // 设置存放在bucket中的路径
            newFilename = StrUtil.isNotBlank(newFilename) ? newFilename : originalFilename;
            String objectName = StrUtil.isNotBlank(targetFolder) ? targetFolder + "/" + newFilename : newFilename;

            //上传文件
            String contentType = ViewContentTypeEnum.getContentType(newFilename);
            log.info("上传文件后缀:{}", contentType);
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .bucket(bucketName)
                    .stream(fis, fis.available(), -1)
                    .contentType(contentType)
                    .object(objectName)
                    .build();
            minioClient.putObject(putObjectArgs);
            String urlPath;
            if (ViewContentTypeEnum.isImage(contentType)) {
                urlPath = minioClient.getPresignedObjectUrl(
                        GetPresignedObjectUrlArgs.builder()
                                .method(Method.GET)
                                .bucket(bucketName)
                                .object(objectName)
                                .build());
            } else {
                urlPath = minioProperties.getEndpoint() + "/" + bucketName + "/" + objectName;
            }
            return urlPath;
        } catch (Exception e) {
            log.error("minio put file error.", e);
            throw new RuntimeException("上传文件失败");
        }
    }


    @Override
    public void delete(String pathUrl) {
        try {
            String key = pathUrl.replace(minioProperties.getEndpoint()+"/","");
            int index = key.indexOf("/");
            String bucket = key.substring(0, index);
            String filePath = key.substring(index + 1);
            RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder().bucket(bucket).object(filePath).build();
            minioClient.removeObject(removeObjectArgs);
        } catch (Exception e) {
            log.error("文件删除失败:{}", pathUrl);
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getFile(String pathUrl)  {
        String key = pathUrl.replace(minioProperties.getEndpoint()+"/","");
        int index = key.indexOf("/");
        String bucket = key.substring(0, index);
        String filePath = key.substring(index + 1);
        InputStream inputStream = null;
        try {
            //找到minio中的指定资源，返回InputStream
            GetObjectArgs objectArgs = GetObjectArgs.builder().bucket(minioProperties.getBucketName()).object(filePath).build();
            inputStream = minioClient.getObject(objectArgs);
        } catch (Exception e) {
            log.error("minio down file error.  pathUrl:{}",pathUrl);
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while (true) {
            try {
                if (!((rc = inputStream.read(buff, 0, 100)) > 0)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteArrayOutputStream.write(buff, 0, rc);
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void downloadFile(String pathUrl, HttpServletResponse response, String filename) {
        InputStream in = null;
        OutputStream out = null;
        try {
            //从pathUrl中获取bucketName和filePath
            String key = pathUrl.replace(minioProperties.getEndpoint() + "/", "");
            int index = key.indexOf("/");
            String bucket = key.substring(0, index);
            String filePath = key.substring(index + 1);

            //找到minio中的指定资源，返回InputStream
            GetObjectArgs objectArgs = GetObjectArgs.builder().bucket(minioProperties.getBucketName()).object(filePath).build();
            in = minioClient.getObject(objectArgs);

            int length = 0;
            byte[] buffer = new byte[1024];
            out = response.getOutputStream();
            response.reset();
            response.addHeader("Content-Disposition", " attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
            response.setContentType("application/octet-stream");
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
