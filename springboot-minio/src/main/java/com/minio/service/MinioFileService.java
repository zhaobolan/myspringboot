package com.minio.service;


import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

public interface MinioFileService {
    /**
     * 上传文件
     *
     * @param file         待上传文件路径
     * @param targetFolder 文件要上传到minio的位置
     * @param newFilename     文件名(可以给上传到服务器的文件指定新名词，若不指定则使用旧名称)
     * @return 文件全路径
     */
    String uploadFile(MultipartFile file, String targetFolder, String newFilename);

    /**
     * 删除文件
     *
     * @param pathUrl 文件全路径
     */
    void delete(String pathUrl);

    /**
     * 下载文件
     * @param pathUrl  文件全路径
     * @return  文件流
     *
     */
    byte[] getFile(String pathUrl);

    /**
     * 下载文件
     * @param pathUrl  文件全路径
     * @param filename 保存的文件名
     *
     */
    void downloadFile(String pathUrl, HttpServletResponse response, String filename);
}
