package com.file.service;

import com.file.entity.SystemFile;
import com.baomidou.mybatisplus.extension.service.IService;
import com.file.entity.SystemFilePart;
import com.file.util.R;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface SystemFileService extends IService<SystemFile> {

    /**
     * 判断文件是否已经上传过
     *
     * @param md5      文件md5
     * @param fileName 待上传文件名称
     * @param bizType  业务类型
     * @return 响应结果
     * @date 2024/6/27 11:27
     */
    R<SystemFile> isUpload(String md5, String fileName, String bizType);

    /**
     * 上传文件
     *
     * @param file    待上传文件
     * @param bizType 业务类型
     * @return 文件上传记录
     */
    R<SystemFile> upload(MultipartFile file, String bizType);

    /**
     * 根据文件ID下载文件
     *
     * @param id 文件id
     */
    void downloadById(Long id);

    /**
     * 获取分片上传uploadId
     *
     * @param fileName 文件名称
     * @param fileSzie 文件大小KB
     * @param partTotal 分片总数
     * @param md5      文件md5值
     * @param bizType  业务类型
     * @return 上传文件信息
     */
    R<SystemFile> getUploadId(String fileName, Long fileSzie, Integer partTotal, String md5, String bizType);

    /**
     * 上传文件分片
     *
     * @param file       待上传文件
     * @param partNumber 分片序号
     * @param md5        分片文件md5值
     * @param fileId   文件id
     * @return 是否上传成功
     */
    R<String> uploadPart(MultipartFile file, Integer partNumber, String md5, Long fileId);

    /**
     * 获取已上传的分片
     *
     * @param fileId 文件ID
     * @return 已上传的分片
     */
    R<List<SystemFilePart>> getUploadedPart(Long fileId);

    /**
     * 获取已上传的分片
     *
     * @param fileId 文件id
     * @return 已上传的分片
     */
    R<SystemFile> completePart(Long fileId);

    /**
     * 获取已上传的分片
     *
     * @param fileId 文件id
     * @return 已上传的分片
     */
    R<String> abortPartUpload(Long fileId);

    /**
     * 删除文件
     *
     * @param id 文件id
     * @return 是否删除成功
     */
    R<String> delete(Long id);
}
