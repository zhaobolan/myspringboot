package com.file.service.impl;

import com.file.entity.SystemFilePart;
import com.file.mapper.SystemFilePartMapper;
import com.file.service.SystemFilePartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 大文件分片上传表 服务实现类
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-27
 */
@Service
public class SystemFilePartServiceImpl extends ServiceImpl<SystemFilePartMapper, SystemFilePart> implements SystemFilePartService {

}
