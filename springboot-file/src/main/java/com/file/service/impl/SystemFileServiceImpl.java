package com.file.service.impl;

import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.file.dto.FileChunkCompleteDTO;
import com.file.entity.SystemFile;
import com.file.entity.SystemFilePart;
import com.file.mapper.SystemFileMapper;
import com.file.mapper.SystemFilePartMapper;
import com.file.service.SystemFileService;
import com.file.strategy.FileChunkStrategy;
import com.file.strategy.FileStrategy;
import com.file.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SystemFileServiceImpl extends ServiceImpl<SystemFileMapper, SystemFile> implements SystemFileService {

    @Autowired
    private FileStrategy fileStrategy;
    @Autowired
    private FileChunkStrategy fileChunkStrategy;
    @Resource
    private SystemFilePartMapper systemFilePartMapper;

    @Override
    public R<SystemFile> isUpload(String md5, String fileName, String bizType) {
        // 根据md5查询上传记录
        List<SystemFile> fileList = this.list(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getFileMd5, md5));
        if (fileList.isEmpty()) {
            log.info("该文件并未上传过:{}", fileName);
            return R.success(null);
        }

        // 大文件,则先判断是否完成上传了
        SystemFile byMd5 = fileList.get(0);
        if (StrUtil.isNotBlank(byMd5.getUploadId()) && byMd5.getStatus() == 0) {
            log.info("该大文件并未上传成功:{}", fileName);
            return R.success(null);
        }

        // 上传成功则生成新的上传记录
        SystemFile systemFile = new SystemFile();
        BeanUtils.copyProperties(byMd5, systemFile);
        systemFile.setId(IdUtil.getSnowflakeNextId());
        systemFile.setBizType(bizType);
        systemFile.setSubmittedFileName(fileName);
        systemFile.setCreateTime(LocalDateTime.now());
        this.save(systemFile);
        return R.success(systemFile);
    }

    @Override
    public R<SystemFile> upload(MultipartFile file, String bizType) {
        SystemFile upload = fileStrategy.upload(file, bizType);
        this.save(upload);
        return R.success(upload);
    }

    @Override
    public void downloadById(Long id) {
        SystemFile byId = this.getById(id);
        fileStrategy.download(byId.getRelativePath(), byId.getSubmittedFileName());
    }

    @Override
    public R<SystemFile> getUploadId(String fileName, Long fileSzie, Integer partTotal, String md5, String bizType) {
        // 初始化上传
        String uploadId = fileChunkStrategy.initMultipartUpload(fileName, bizType);

        // 构建文件对象
        SystemFile systemFile = new SystemFile();
        systemFile.setId(IdUtil.getSnowflakeNextId());
        systemFile.setSubmittedFileName(fileName);
        systemFile.setSize(fileSzie);
        systemFile.setBizType(bizType);
        systemFile.setCreateTime(LocalDateTime.now());
        systemFile.setFileMd5(md5);
        systemFile.setUploadId(uploadId);
        systemFile.setPartTotal(partTotal);
        systemFile.setFilename(fileName);
        systemFile.setStatus(0);
        this.save(systemFile);
        return R.success(systemFile);
    }

    @Override
    public R<String> uploadPart(MultipartFile file, Integer partNumber, String md5, Long fileId) {
        // 判断该uploadId是否合法
        SystemFile byMd5 = this.getOne(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getId, fileId));
        if (Objects.isNull(byMd5)) {
            return R.fail("uploadId不存在,请先获取!");
        }
        if (Objects.nonNull(byMd5.getStatus()) && byMd5.getStatus() == 1) {
            return R.fail("该文件已上传成功,不可再上传分片!");
        }

        // 判断分片是否被篡改
//        if (!md5.equals(FileUploadUtil.getMD5OfFile(file))) {
//            return R.fail("该分片md5值与上传md5值不一致!");
//        }

        // 判断该分片是否已经上传过了
        List<SystemFilePart> systemFileParts = systemFilePartMapper.selectList(Wrappers.<SystemFilePart>lambdaQuery()
                .eq(SystemFilePart::getFileId, fileId)
                .eq(SystemFilePart::getPartNum, partNumber)
        );
        if (!systemFileParts.isEmpty()) {
            return R.fail("该分片已上传,无需重复上传!");
        }

        // 上传文件分片
        boolean result = fileChunkStrategy.uploadPart(byMd5.getUploadId(), partNumber, file, byMd5.getBizType(), byMd5.getSubmittedFileName());
        if(result) {
            // 保存上传记录
            SystemFilePart systemFilePart = new SystemFilePart();
            systemFilePart.setId(IdUtil.getSnowflakeNextId());
            systemFilePart.setFileId(byMd5.getId());
            systemFilePart.setPartNum(partNumber);
            systemFilePart.setSize(file.getSize());
            systemFilePart.setFileMd5(md5);
            systemFilePart.setCreateTime(LocalDateTime.now());
            systemFilePartMapper.insert(systemFilePart);
        }
        return R.success("true");
    }

    @Override
    public R<List<SystemFilePart>> getUploadedPart(Long fileId) {
        // 校验该文件是否传过
        SystemFile byMd5 = this.getOne(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getId, fileId));
        if (Objects.isNull(byMd5) || byMd5.getStatus() == 1) {
            return R.fail("uploadId无效,请重新获取!");
        }

        // 从数据库查询该文件所有已经上传的分片
        List<SystemFilePart> parts = systemFilePartMapper.selectList(Wrappers.<SystemFilePart>lambdaQuery()
                .eq(SystemFilePart::getFileId, byMd5.getId()));

        // 从文件服务器查询所有已经上传的分片
        String filePartStr = fileChunkStrategy.listPart(byMd5.getUploadId(), byMd5.getSubmittedFileName(), byMd5.getBizType());
        JSONArray jsonArray = JSONUtil.parseArray(filePartStr);

        // 比较数据库和文件服务器的分片
        Map<String, String> collect =
                jsonArray.stream().map(o -> (JSONObject) o)
                        .collect(Collectors.toMap(o -> o.getStr("partNumber"), o -> o.getStr("etag"), (a, b) -> b));

        List<SystemFilePart> updates = new ArrayList<>();
        List<SystemFilePart> removes = new ArrayList<>();
        for (SystemFilePart part : parts) {
            Integer partNum = part.getPartNum();
            String etag = collect.get(String.valueOf(partNum));
            if (StrUtil.isNotBlank(etag)) {
                part.setEtag(etag);
                updates.add(part);
            } else {
                removes.add(part);
            }
        }

        // 更新数据库
        systemFilePartMapper.updateById(updates);
        // 移除上传失败的分片
        if (!removes.isEmpty()) {
            systemFilePartMapper.deleteByIds(removes.stream().map(SystemFilePart::getId).collect(Collectors.toList()));
        }
        return R.success(updates);
    }

    @Override
    public R<SystemFile> completePart(Long fileId) {
        // 校验参数有效性
        SystemFile byMd5 = this.getOne(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getId, fileId));
        if (Objects.isNull(byMd5)) {
            return R.fail("请先获取文件uploadId!");
        }

        if (byMd5.getStatus() == 1) {
            return R.fail("文件已上传成功，无需再合并!");
        }

        // 从文件服务器查询所有已经上传的分片
        String filePartStr = fileChunkStrategy.listPart(byMd5.getUploadId(), byMd5.getSubmittedFileName(), byMd5.getBizType());
        JSONArray jsonArray = JSONUtil.parseArray(filePartStr);
        Map<String, String> collect =
                jsonArray.stream().map(o -> (JSONObject) o)
                        .collect(Collectors.toMap(o -> o.getStr("partNumber"), o -> o.getStr("etag")));
        if (byMd5.getPartTotal() != collect.size()) {
            return R.fail("该文件分片缺失,不能合并!");
        }

        // 合并文件
        FileChunkCompleteDTO record = new FileChunkCompleteDTO();
        record.setUploadId(byMd5.getUploadId());
        record.setPartETags(filePartStr);
        record.setFileName(byMd5.getSubmittedFileName());
        record.setBizType(byMd5.getBizType());
        boolean part = fileChunkStrategy.completePart(record, byMd5);
        if (part) {
            // 当前文件上传记录标记为成功
            byMd5.setStatus(1);
            updateById(byMd5);
        }
        return R.success(byMd5);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<String> abortPartUpload(Long fileId) {
        // 校验参数有效性
        SystemFile byMd5 = this.getOne(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getId, fileId));
        if (Objects.isNull(byMd5)) {
            return R.fail("文件不存在!");
        }

        // 删除文件服务器分片
        fileChunkStrategy.abortPartUpload(byMd5.getUploadId(), byMd5.getSubmittedFileName(), byMd5.getBizType());

        // 删除本地上传文件记录
        removeById(byMd5.getId());

        // 删除本地分片记录信息
        systemFilePartMapper.delete(Wrappers.<SystemFilePart>lambdaQuery().eq(SystemFilePart::getFileId, byMd5.getId()));
        return R.success("ok");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<String> delete(Long id) {
        SystemFile byId = getById(id);
        if (Objects.isNull(byId)) {
            return R.fail("待删除文件不存在!");
        }
        // 删除文件上传记录
        removeById(id);

        // 删除大文件对应的分片记录
        if (Objects.nonNull(byId.getUploadId())) {
            systemFilePartMapper.delete(Wrappers.<SystemFilePart>lambdaQuery().eq(SystemFilePart::getFileId, id));
        }

        // 若该文件没有被秒传过，则删除文件服务器上对应的文件
        List<SystemFile> fileList = this.list(Wrappers.<SystemFile>lambdaQuery().eq(SystemFile::getFileMd5, byId.getFileMd5()));
        if(fileList.isEmpty()) {
            fileStrategy.delete(Collections.singletonList(byId));
        }
        return R.success("ok");
    }
}
