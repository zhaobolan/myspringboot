package com.file.service;

import com.file.entity.SystemFilePart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 大文件分片上传表 服务类
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-27
 */
public interface SystemFilePartService extends IService<SystemFilePart> {

}
