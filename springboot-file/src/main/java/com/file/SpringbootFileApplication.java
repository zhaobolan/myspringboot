package com.file;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@MapperScan("com.file.mapper")
@Configuration
public class SpringbootFileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootFileApplication.class, args);
    }


    @Bean
    public MultipartConfigElement multipartConfigElement(){
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofMegabytes(500L));
        factory.setMaxRequestSize(DataSize.ofMegabytes(500L));
        return factory.createMultipartConfig();
    }

}
