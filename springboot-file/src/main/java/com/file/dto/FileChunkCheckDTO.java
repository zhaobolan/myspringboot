package com.file.dto;

import lombok.Data;
import lombok.ToString;

/**
 * 分片检测参数
 *
 * @author gmis
 * @date 2018/08/28
 */
@Data
@ToString
public class FileChunkCheckDTO {

    private Long size;

    private String name;

    private Integer chunkIndex;
}
