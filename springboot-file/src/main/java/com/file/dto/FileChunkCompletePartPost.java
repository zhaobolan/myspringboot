package com.file.dto;

import lombok.Data;

/**
 * 分片合并请求参数
 *
 * @author wyj
 * @date 2024/6/19 17:32
 */
@Data
public class FileChunkCompletePartPost {

    private String uploadId;

    private String partETags;

    private String fileName;

    private String bizType;
}
