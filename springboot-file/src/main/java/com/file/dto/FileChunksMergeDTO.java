package com.file.dto;

import lombok.Data;
import lombok.ToString;

/**
 * 封建分片合并DTO
 *
 * @author gmis
 * @date 2018/08/28
 */
@Data
@ToString
public class FileChunksMergeDTO {

    private String name;

    private String submittedFileName;

    private String md5;

    private Integer chunks;

    private String ext;

    private Long folderId;

    private Long size;

    private String contextType;
}
