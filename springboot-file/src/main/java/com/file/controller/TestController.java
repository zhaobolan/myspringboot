package com.file.controller;

import com.file.properties.FileServerProperties;
import com.file.util.ViewContentTypeEnum;
import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
@RestController
public class TestController {

    @Autowired
    protected FileServerProperties fileProperties;

    /**
     * 文件转发(从文件服务器下载文件然后输出到浏览器，做到在线预览和在线下载效果)
     * 虽然文件服务器本身就可以预览和下载，但是有时候文件服务器并不会暴露到公网，此时我们就可以通过网关和自己定义的文件转发服务来实现公网的文件访问
     *
     * @param objectName
     * @param response
     */
    @GetMapping("/downloadFile")
    public void downloadFile(@RequestParam(value = "objectName") String objectName, HttpServletResponse response) {
        // 获取客户端
        FileServerProperties.Minio minio = fileProperties.getMinio();
        MinioClient minioClient = MinioClient
                .builder()
                .credentials(minio.getAccessKey(), minio.getSecretKey())
                .endpoint(minio.getEndpoint())
                .build();

        String fileName = objectName.substring(objectName.lastIndexOf("/") + 1);
        response.setContentType(ViewContentTypeEnum.getContentType(fileName));
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");

        try (
                InputStream stream =
                        minioClient.getObject(GetObjectArgs.builder().bucket(minio.getBucketName()).object(objectName).build());
                BufferedInputStream in = new BufferedInputStream(stream);
                OutputStream out = response.getOutputStream()
        ) {

            byte[] buffer = new byte[1024];
            int bytesRead;

            // 读取输入流并写入输出流，直到没有更多数据
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
