package com.file.controller;

import com.file.entity.SystemFile;
import com.file.entity.SystemFilePart;
import com.file.service.SystemFileService;
import com.file.util.R;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;


/**
 *秒传 分片上传  断点续传  参考链接:
 * https://cloud.tencent.com/developer/article/2160611
 *
 * 小文件上传步骤:
 * 1.前端计算该文件的md5值
 * 2.调用isUpload接口判断该文件是否上传过,若传过,则无需再传
 * 3.若没有上传过,则调用upload接口上传文件即可
 *
 * 大文件分片上传步骤:
 * 1.前端计算该文件的md5,然后请求后端isUpload接口,判断该文件是否已经上传过,若上传过,则直接返回文件url并生成上传信息,上传文件结束
 * 2.若没有完全没有上传过,则前端对文件进行分片,然后调用后端接口getUploadId()获取uploadId,接着再上传所有分片,上传完后再进行文件合并操作
 * 3.若第一步发现已经上传了部分,则前端依旧对文件进行分片,然后获取之前已经上传成功过得分片,这部分分片就不需要再上传,接着上传剩余的分片即可
 * 4.所有分片上传完成后,进行文件合并操作,合并完成后返回文件url
 *
 * minio注意事项:
 * 1.minio分片文件最小必须得5M
 * 2minio的桶的权限设置成public吧，不然会有好多权限问题
 *
 * @author weiyiji
 * @since 2024-06-24
 */
@RestController
@RequestMapping("/systemFile")
public class SystemFileController {

    @Resource
    private SystemFileService systemFileService;

    /**
     * 判断指定文件是否上传过
     */
    @GetMapping("/isUpload")
    public R<SystemFile> isUpload(@RequestParam(value = "md5") String md5,
                                  @RequestParam(value = "fileName") String fileName,
                                  @RequestParam("bizType") String bizType) {
        return systemFileService.isUpload(md5, fileName, bizType);
    }

    /**
     * 小文件上传
     */
    @PostMapping("/upload")
    public R<SystemFile> upload(@RequestParam(value = "file") MultipartFile file, @RequestParam("bizType") String bizType) {
        return systemFileService.upload(file, bizType);
    }

    /**
     * 根据文件ID下载文件到根目录
     */
    @PostMapping(value = "/downloadById", produces = "application/octet-stream")
    public void download(@RequestParam("id") Long id) {
        systemFileService.downloadById(id);
    }

    /**
     * 获取分片上传uploadId
     */
    @GetMapping("/getUploadId")
    public R<SystemFile> getUploadId(@RequestParam("fileName") String fileName,
                                     @RequestParam("fileSize") Long fileSize,
                                     @RequestParam("partTotal") Integer partTotal,
                                     @RequestParam("md5") String md5,
                                     @RequestParam("bizType") String bizType) {
        return systemFileService.getUploadId(fileName, fileSize, partTotal, md5, bizType);
    }

    /**
     * 上传分片
     */
    @PostMapping("/uploadPart")
    public R<String> uploadPart(@RequestParam(value = "file") MultipartFile file,
                                @RequestParam(value = "partNumber") Integer partNumber,
                                @RequestParam("md5") String md5,
                                @RequestParam("fileId") Long fileId) {
        return systemFileService.uploadPart(file, partNumber, md5, fileId);
    }

    /**
     * 获取已经上传的分片
     */
    @GetMapping("/getUploadedPart")
    public R<List<SystemFilePart>> getUploadedPart(@RequestParam("fileId") Long fileId) {
        return systemFileService.getUploadedPart(fileId);
    }

    /**
     * 分片合并
     */
    @PostMapping("/completePart")
    public R<SystemFile> completePart(@RequestParam("fileId") Long fileId) {
        return systemFileService.completePart(fileId);
    }

    /**
     * 分片上传终止(有些场景可能会涉及到上传失败后,则终止分片,这时就需要将以前的分片数据清除掉)
     */
    @PostMapping("/abortPartUpload")
    public R<String> abortPartUpload(@RequestParam("fileId") Long fileId) {
        return systemFileService.abortPartUpload(fileId);
    }

    /**
     * 文件删除
     */
    @PostMapping("/delete")
    public R<String> delete(@RequestParam("id") Long id) {
        return systemFileService.delete(id);
    }
}
