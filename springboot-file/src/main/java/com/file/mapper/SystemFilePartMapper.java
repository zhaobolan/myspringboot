package com.file.mapper;

import com.file.entity.SystemFilePart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 大文件分片上传表 Mapper 接口
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-27
 */
public interface SystemFilePartMapper extends BaseMapper<SystemFilePart> {

}
