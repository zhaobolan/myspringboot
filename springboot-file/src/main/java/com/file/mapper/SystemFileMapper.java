package com.file.mapper;

import com.file.entity.SystemFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件表 Mapper 接口
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
public interface SystemFileMapper extends BaseMapper<SystemFile> {

}
