package com.file.properties;

/**
 * 文件 存储类型 枚举
 */
public enum FileStorageType {
    /**
     * 本地
     */
    LOCAL,
    /**
     * FastDFS
     */
    FAST_DFS,
    /**
     * 阿里云
     */
    ALI,
    /**
     * 七牛oss
     */
    QINIU,
    /**
     * 腾讯云
     */
    TENCENT_COS,
    /**
     * 电信云
     */
    CTCC,
    /**
     * 移动云
     */
    CMCC,
    /**
     * 华为云
     */
    HUAWEI,
    /**
     * MINIO
     */
    MINIO;

    public boolean eq(FileStorageType type) {
        for (FileStorageType t : FileStorageType.values()) {
            return t.equals(type);
        }
        return false;
    }
}
