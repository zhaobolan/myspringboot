package com.file.properties;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Setter
@Getter
@ConfigurationProperties(prefix = "gmis.file")
@Component
@Primary
public class FileServerProperties {

    private FileStorageType type = FileStorageType.LOCAL;

    private Ali ali;

    private Tencent tencent;

    private Ctcc ctcc;

    private Cmcc cmcc;

    private Huawei huawei;

    private Minio minio;

    public FileStorageType getType() {
        return type;
    }

    @Data
    public static class Ali {
        private String uriPrefix;
        private String endpoint;
        private String accessKeyId;
        private String accessKeySecret;
        private String bucketName;
    }

    @Data
    public static class Tencent {
        private String appId;
        private String secretId;
        private String secretKey;
        private String reginName;
        private String bucketName;
        private String path;
    }

    @Data
    public static class Ctcc {
        private String accessId;
        private String accessKey;
        private String endpoint;
        private String bucketName;
    }

    @Data
    public static class Cmcc {
        private String accesskeyIa;
        private String accessKeySecret;
        private String endpoint;
        private String bucketName;
    }

    @Data
    public  static class Huawei {
        private String endpoint;
        private String ak;
        private String sk;
        private String authType;
        private String bucketName;
    }

    @Data
    public  static class Minio {
        private String endpoint;
        private String bucketName;
        private String secretKey;
        private String accessKey;
    }
}
