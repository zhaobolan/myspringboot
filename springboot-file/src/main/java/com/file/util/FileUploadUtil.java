package com.file.util;


import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件上传工具类
 */
public class FileUploadUtil {

    /**
     * 分片文件大小(10M)
     */
    public static final int EXPORT_SIZE_OF_FILES = 1024 * 1024 * 10;

    /**
     * 分片文件
     *
     * @param file 待分片文件
     * @return key 为分片文件序号，value 为分片文件名
     */
    public static Map<Integer, String> splitFile(File file) throws IOException {
        Map<Integer, String> fileNameMap = new HashMap<>();
        int partCounter = 1;
        byte[] buffer = new byte[EXPORT_SIZE_OF_FILES];
        String fileName = file.getName();
        try (FileInputStream fis = new FileInputStream(file);
             BufferedInputStream bis = new BufferedInputStream(fis)) {
            int bytesAmount;
            while ((bytesAmount = bis.read(buffer)) > 0) {
                int num = partCounter++;
                String filePartName = String.format("%s.%03d", fileName, num);
                File newFile = new File(file.getParent(), filePartName);
                try (FileOutputStream out = new FileOutputStream(newFile)) {
                    out.write(buffer, 0, bytesAmount);
                }
                fileNameMap.put(num, filePartName);
            }
        }
        return fileNameMap;
    }

    /**
     * 获取文件MD5
     *
     * @param file 待获取md5值文件
     * @return 文件md5值
     */
    public static String getMD5OfFile(File file) {
        String md5 = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            try (FileInputStream fis = new FileInputStream(file)) {
                byte[] dataBytes = new byte[1024];
                int nread = 0;
                while ((nread = fis.read(dataBytes)) != -1) {
                    digest.update(dataBytes, 0, nread);
                }
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            md5 = bigInt.toString(16);
            while (md5.length() < 32) {
                md5 = "0" + md5;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return md5;
    }

    /**
     * 获取文件MD5
     *
     * @param file 待获取md5值文件
     * @return 文件md5值
     */
    public static String getMD5OfFile(MultipartFile file) {
        String md5 = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            try (FileInputStream fis = (FileInputStream) file.getInputStream()) {
                byte[] dataBytes = new byte[1024];
                int nread = 0;
                while ((nread = fis.read(dataBytes)) != -1) {
                    digest.update(dataBytes, 0, nread);
                }
            }
            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            md5 = bigInt.toString(16);
            while (md5.length() < 32) {
                md5 = "0" + md5;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return md5;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(getMD5OfFile(new File("D:\\aaa.xlsx")));
        System.out.println(splitFile(new File("D:\\aaa.xlsx")));
    }
}
