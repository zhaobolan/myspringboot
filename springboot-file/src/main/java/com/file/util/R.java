package com.file.util;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@SuppressWarnings("ALL")
@Accessors(chain = true)
public class R<T> {
    public static final String DEF_ERROR_MESSAGE = "系统繁忙，请稍候再试";
    public static final String DEF_ERROR_MESSAGE_En = "The system is busy. Please try again later";
    public static final String HYSTRIX_ERROR_MESSAGE = "请求超时，请稍候再试";
    public static final String HYSTRIX_ERROR_MESSAGE_En = "The request timed out. Please try again later";
    public static final int SUCCESS_CODE = 0;
    public static final int FAIL_CODE = -1;
    public static final int TIMEOUT_CODE = -2;
    public static final int VALID_EX_CODE = -9;
    public static final int OPERATION_EX_CODE = -10;

    private int code;

    @JsonIgnore
    private Boolean defExec = true;

    private T data;

    private String msg = "ok";

    private String debugMsg = "调试信息";

    private String path;

    private Map<String, Object> extra;

    private String traceId;

    private long timestamp = System.currentTimeMillis();

    private R() {
        super();
    }

    public R(int code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.defExec = false;
    }

    public R(int code, T data, String msg, String debugMsg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.debugMsg = debugMsg;
        this.defExec = false;
    }

    public R(int code, T data, String msg, boolean defExec) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.defExec = defExec;
    }

    public R(int code, T data, String msg, boolean defExec, String debugMsg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.debugMsg = debugMsg;
        this.defExec = defExec;
    }

    public static <E> R<E> result(int code, E data, String msg) {
        return new R<>(code, data, msg);
    }

    public static <E> R<E> success(E data) {
        return new R<>(SUCCESS_CODE, data, "ok");
    }

    public static R<Boolean> success() {
        return new R<>(SUCCESS_CODE, true, "ok");
    }

    public static R<Boolean> success(Boolean falg, String message) {
        return new R<>(SUCCESS_CODE, falg, message);
    }

    public static <E> R<E> successDef(E data) {
        return new R<>(SUCCESS_CODE, data, "ok", true);
    }

    public static <E> R<E> successDef() {
        return new R<>(SUCCESS_CODE, null, "ok", true);
    }

    public static <E> R<E> successDef(E data, String msg) {
        return new R<>(SUCCESS_CODE, data, msg, true);
    }

    public static <E> R<E> success(E data, String msg) {
        return new R<>(SUCCESS_CODE, data, msg);
    }
    public static <E> R<E> fail(String msg) {
        return new R<>(FAIL_CODE, null,msg);
    }

    public R<T> put(String key, Object value) {
        if (this.extra == null) {
            this.extra = new HashMap<>(10);
        }
        this.extra.put(key, value);
        return this;
    }

    public Boolean getIsSuccess() {
        return this.code == SUCCESS_CODE || this.code == 200;
    }

    public Boolean getIsError() {
        return !getIsSuccess();
    }


    public T getData(String msg) {
        if (getIsError()) {
            throw new RuntimeException(msg);
        }
        return data;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTraceId() {
        return traceId;
    }
}
