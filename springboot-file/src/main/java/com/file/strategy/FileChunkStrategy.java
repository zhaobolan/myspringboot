package com.file.strategy;


import com.file.dto.FileChunkCompleteDTO;
import com.file.entity.SystemFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * 大文件分片处理策略类
 */
public interface FileChunkStrategy {

    /**
     * 获取分片上传uploadId
     */
    default String initMultipartUpload(String fileName, String bizType) {
        return "";
    }

    /**
     * 用于获取已上传的分片
     */
    default String listPart(String uploadId, String fileName, String bizType) {
        return null;
    }

    /**
     * 分块上传
     */
    default boolean uploadPart(String uploadId, int partNumber, MultipartFile multipartFile, String bizType,String fileName) {
        return false;
    }

    /**
     * 完成分块上传
     */
    default boolean completePart(FileChunkCompleteDTO record, SystemFile systemFile) {
        return false;
    }

    /**
     * 终止分块上传
     */
    default void abortPartUpload(String uploadId, String fileName, String bizType) {
    }
}