package com.file.strategy;

import com.file.entity.SystemFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * 文件策略接口
 *
 * @author wyj
 * @date 2024/6/21 15:46
 */
public interface FileStrategy {
    /**
     * 文件上传
     *
     * @param file    文件
     * @param bizType 业务类型
     * @date 2019-05-06 16:38
     */
    SystemFile upload(MultipartFile file, String bizType);

    /**
     * 下载文件
     *
     * @param key      文件相对位置
     * @param downName 下载后的文件名
     */
    void download(String key, String downName);

    /**
     * 删除文件
     *
     * @param list 文件id集合
     */
    boolean delete(List<SystemFile> list);
}