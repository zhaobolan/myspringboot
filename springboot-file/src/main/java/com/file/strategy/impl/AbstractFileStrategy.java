package com.file.strategy.impl;

import cn.hutool.core.util.IdUtil;
import com.file.entity.SystemFile;
import com.file.properties.FileServerProperties;
import com.file.strategy.FileStrategy;
import com.file.util.FileUploadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


/**
 * 文件抽象策略 处理类
 */
@Slf4j
public abstract class AbstractFileStrategy implements FileStrategy {

    private static final String FILE_SPLIT = ".";

    @Autowired
    protected FileServerProperties fileProperties;

    @Override
    public SystemFile upload(MultipartFile multipartFile, String bizType) {
        try {
            if (!Objects.requireNonNull(multipartFile.getOriginalFilename()).contains(FILE_SPLIT)) {
                throw new RuntimeException("缺少后缀名");
            }

            // 构建文件对象
            SystemFile file = new SystemFile();
            file.setId(IdUtil.getSnowflakeNextId());
            file.setSubmittedFileName(multipartFile.getOriginalFilename());
            file.setSize(multipartFile.getSize());
            file.setBizType(bizType);
            file.setCreateTime(LocalDateTime.now());
            file.setFileMd5(FileUploadUtil.getMD5OfFile(multipartFile));

            // 执行文件的上传
            uploadFile(file, multipartFile, bizType);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("文件上传失败");
        }
    }


    @Override
    public abstract void download(String key, String downName);

    /**
     * 由子类型执行具体的上传操作
     */
    protected abstract void uploadFile(SystemFile file, MultipartFile multipartFile, String bizType) throws Exception;

    @Override
    public boolean delete(List<SystemFile> list) {
        if (list.isEmpty()) {
            return true;
        }
        boolean flag = false;
        for (SystemFile file : list) {
            try {
                delete(list, file);
                flag = true;
            } catch (Exception e) {
                log.error("删除文件失败", e);
            }
        }
        return flag;
    }

    /**
     * 由子类型执行具体的删除操作
     */
    protected abstract void delete(List<SystemFile> list, SystemFile file);
}
