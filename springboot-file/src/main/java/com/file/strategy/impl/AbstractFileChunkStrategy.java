package com.file.strategy.impl;

import com.file.properties.FileServerProperties;
import com.file.strategy.FileChunkStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 文件分片处理 抽象策略类
 */
@Slf4j
public abstract class AbstractFileChunkStrategy implements FileChunkStrategy {

    @Autowired
    protected FileServerProperties fileProperties;

}
