package com.file.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 文件表
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-24
 */
@Data
@TableName("system_file")
public class SystemFile implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 原始文件名
     */
    private String submittedFileName;

    /**
     * 文件访问链接
     */
    private String url;

    /**
     * 文件大小单位字节
     */
    private Long size;

    /**
     * 相对路径
     */
    private String relativePath;

    /**
     * md5值
     */
    private String fileMd5;

    /**
     * 文件类型
     * 取上传文件的值
     */
    private String contextType;

    /**
     * 唯一文件名
     */
    private String filename;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 业务类型
     */
    private String bizType;

    /**
     * 大文件分片上传ID
     */
    private String uploadId;

    /**
     * 大文件分片总数
     */
    private Integer partTotal;

    /**
     * 大文件上传状态：0-未完成，1-已完成
     */
    private Integer status;

}
