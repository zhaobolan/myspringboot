package com.file.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * <p>
 * 大文件分片上传表
 * </p>
 *
 * @author weiyiji
 * @since 2024-06-27
 */
@Data
@TableName("system_file_part")
public class SystemFilePart implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Long id;

    /**
     * 原文件ID
     */
    private Long fileId;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * md5值
     */
    private String fileMd5;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 分片序号
     */
    private Integer partNum;

    /**
     * 分片序号
     */
    private String etag;
}
