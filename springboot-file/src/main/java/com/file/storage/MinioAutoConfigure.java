package com.file.storage;

import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.file.config.CustomMinioClient;
import com.file.dto.FileChunkCompleteDTO;
import com.file.entity.SystemFile;
import com.file.properties.FileServerProperties;
import com.file.strategy.impl.AbstractFileChunkStrategy;
import com.file.strategy.impl.AbstractFileStrategy;
import com.file.util.ViewContentTypeEnum;
import com.google.common.collect.HashMultimap;
import io.minio.*;
import io.minio.messages.Part;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Configuration
@EnableConfigurationProperties(FileServerProperties.class)
@ConditionalOnProperty(name = "gmis.file.type", havingValue = "MINIO")
public class MinioAutoConfigure {

    @SneakyThrows(Exception.class)
    public CustomMinioClient getMinioClient(FileServerProperties.Minio minioProperties) {
        MinioClient minioClient = MinioClient
                .builder()
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
                .endpoint(minioProperties.getEndpoint())
                .build();

        CustomMinioClient customMinioClient = new CustomMinioClient(minioClient);

        // 创建 bucket
        String bucketName = minioProperties.getBucketName();
        boolean isExist = customMinioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        if (!isExist) {
            customMinioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }
        return customMinioClient;
    }

    @Service
    public class MinioServiceImpl extends AbstractFileStrategy {
        @Override
        public void download(String key, String downName) {
            // 获取MinIO客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            MinioClient minioClient = getMinioClient(minio);

            String downPath = "/" + downName;
            GetObjectArgs objectArgs = GetObjectArgs.builder().bucket(minio.getBucketName()).object(key).build();
            try (
                    FilterInputStream inputStream = minioClient.getObject(objectArgs);
                    FileOutputStream fileOutputStream = new FileOutputStream(downPath)
            ) {
                // 缓冲区
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    fileOutputStream.write(buffer, 0, bytesRead);
                }
                fileOutputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void uploadFile(SystemFile file, MultipartFile multipartFile, String bizType) {
            if (multipartFile == null) {
                return;
            }

            MinioClient minioClient;
            try (InputStream fis = multipartFile.getInputStream()) {

                // 获取MinIO客户端
                FileServerProperties.Minio minio = fileProperties.getMinio();
                minioClient = getMinioClient(minio);
                String bucketName = minio.getBucketName();

                // 构建存储文件名称
                String oldFileName = multipartFile.getOriginalFilename();
                String eName = oldFileName.substring(oldFileName.lastIndexOf("."));
                String newFileName = UUID.randomUUID() + eName;
                String relativePath = Paths.get(bizType).toString().replaceAll("\\\\", StrPool.SLASH);
                String objectName = StrPool.SLASH + relativePath + StrPool.SLASH + newFileName;

                // 上传文件到MinIO服务器
                PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                        .bucket(bucketName)
                        .stream(fis, fis.available(), -1)
                        .contentType(ViewContentTypeEnum.getContentType(oldFileName))
                        .object(objectName)
                        .build();
                minioClient.putObject(putObjectArgs);

                String urlPath = minio.getEndpoint() + StrPool.SLASH + bucketName + StrPool.SLASH + relativePath + StrPool.SLASH + newFileName;
                file.setUrl(StrUtil.replace(urlPath, "\\\\", StrPool.SLASH));
                file.setFilename(newFileName);
                file.setRelativePath(StrPool.SLASH + relativePath + StrPool.SLASH + newFileName);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("文件上传报错");
            }
        }

        @Override
        protected void delete(List<SystemFile> list, SystemFile file) {
            // 获取Minio客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            MinioClient minioClient = getMinioClient(minio);

            // 删除文件
            try {
                RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                        .bucket(minio.getBucketName())
                        .object(file.getRelativePath())
                        .build();
                minioClient.removeObject(removeObjectArgs);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("文件删除失败");
            }
        }
    }

    @Service
    public class MinioChunkServiceImpl extends AbstractFileChunkStrategy {

        @Override
        public String initMultipartUpload(String fileName, String bizType) {
            // 获取Minio客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            CustomMinioClient minioClient = getMinioClient(minio);

            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;

            try {
                HashMultimap<String, String> headers = HashMultimap.create();
                headers.put("Content-Type", ViewContentTypeEnum.getContentType(fileName));
                return minioClient.initMultiPartUpload(minio.getBucketName(), null, key, headers, null);
            } catch (Exception e) {
                log.error("文件上传报错:", e);
            }
            return null;
        }

        @Override
        public String listPart(String uploadId, String fileName, String bizType) {
            // 获取Minio客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            CustomMinioClient minioClient = getMinioClient(minio);
            String bucketName = minio.getBucketName();

            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;
            JSONArray jsonArray = new JSONArray();
            try {
                HashMultimap<String, String> headers = HashMultimap.create();
                headers.put("Content-Type", ViewContentTypeEnum.getContentType(fileName));
                ListPartsResponse partResult = minioClient.listMultipart(bucketName, null, key, 1000, 0, uploadId, headers, null);
                for (Part part : partResult.result().partList()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.set("partNumber", part.partNumber());
                    jsonObject.set("etag", part.etag());
                    jsonArray.add(jsonObject);
                }
            } catch (Exception e) {
                log.error("获取已上传分片信息失败:", e);
                e.printStackTrace();
            }
            return jsonArray.toString();
        }

        @Override
        public boolean uploadPart(String uploadId, int partNumber, MultipartFile multipartFile, String bizType, String fileName) {
            // 获取Minio客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            CustomMinioClient minioClient = getMinioClient(minio);
            String bucketName = minio.getBucketName();

            try (InputStream inputStream = multipartFile.getInputStream();
                 BufferedInputStream bis = new BufferedInputStream(inputStream)) {

                String key = StrPool.SLASH + spliceName(fileName, bizType);

                HashMultimap<String, String> headers = HashMultimap.create();
                headers.put("Content-Type", "application/octet-stream");
                minioClient.uploadPart(bucketName, null, key, bis, inputStream.available(), uploadId, partNumber, headers, null);
                return true;
            } catch (Exception e) {
                log.error("文件上传未知异常：", e);
                e.printStackTrace();
                return false;
            }
        }

        @Override
        public boolean completePart(FileChunkCompleteDTO record, SystemFile systemFile) {
            String uploadId = record.getUploadId();
            String partETags = record.getPartETags();
            String bizType = record.getBizType();

            // 获取Minio客户端
            FileServerProperties.Minio minio = fileProperties.getMinio();
            CustomMinioClient minioClient = getMinioClient(minio);
            String bucketName = minio.getBucketName();

            String key = StrPool.SLASH + spliceName(systemFile.getSubmittedFileName(), bizType);

            JSONArray jsonArray = new JSONArray(partETags);
            List<Part> parts = new ArrayList<>();
            for (Object o : jsonArray) {
                JSONObject o1 = (JSONObject) o;
                parts.add(new Part(o1.getInt("partNumber"), o1.getStr("etag")));
            }
            Part[] parts1 = parts.toArray(new Part[0]);

            try {
                HashMultimap<String, String> headers = HashMultimap.create();
                headers.put("Content-Type", "application/octet-stream");
                minioClient.mergeMultipartUpload(bucketName, null, key, uploadId, parts1, null, null);
                String url = minio.getEndpoint() + StrPool.SLASH + bucketName + StrPool.SLASH + key;
                systemFile.setUrl(url);
                systemFile.setRelativePath(key);
                return true;
            } catch (Exception e) {
                log.error("文件上传未知异常：", e);
                return false;
            }
        }

        @Override
        public void abortPartUpload(String uploadId, String fileName, String bizType) {
            try {
                // 获取Minio客户端
                FileServerProperties.Minio minio = fileProperties.getMinio();
                CustomMinioClient minioClient = getMinioClient(minio);
                String bucketName = minio.getBucketName();

                fileName = spliceName(fileName, bizType);
                String key = StrPool.SLASH + fileName;

                HashMultimap<String, String> headers = HashMultimap.create();
                headers.put("Content-Type", "application/octet-stream");
                minioClient.abortMultipartUpload(bucketName, null, key, uploadId, headers, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private String spliceName(String fileName, String bizType) {
            String relativePath = Paths.get(bizType).toString().replaceAll("\\\\", StrPool.SLASH);
            fileName = relativePath + StrPool.SLASH + fileName;
            return fileName;
        }
    }
}
