package com.file.storage;

import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.StrUtil;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.file.dto.FileChunkCompleteDTO;
import com.file.entity.SystemFile;
import com.file.properties.FileServerProperties;
import com.file.strategy.impl.AbstractFileChunkStrategy;
import com.file.strategy.impl.AbstractFileStrategy;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.exception.CosClientException;
import com.qcloud.cos.model.*;
import com.qcloud.cos.region.Region;
import io.minio.messages.Part;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * 腾讯cos
 *
 * @author gmis
 * @date 2019/08/09
 */
@EnableConfigurationProperties(FileServerProperties.class)
@Configuration
@Slf4j
@ConditionalOnProperty(name = "gmis.file.type", havingValue = "TENCENT_COS")
public class TencentCosAutoConfigure {

    @Autowired
    protected FileServerProperties fileProperties;

    private FileServerProperties.Tencent tencent;
    private  String secretId;
    private  String secretKey;
    private  String reginName;
    private  String bucketName;
    private  String path;

    public void init(){
        if(null==tencent){
            this.tencent = fileProperties.getTencent();
            this.secretKey = tencent.getSecretKey();
            this.secretId = tencent.getSecretId();
            this.reginName = tencent.getReginName();
            this.bucketName = tencent.getBucketName();
            this.path = tencent.getPath();
        }
    }

    public COSClient getCOSClient(){
        init();
        // 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials cred = new BasicCOSCredentials(secretId, secretKey);
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        ClientConfig clientConfig = new ClientConfig(new Region(reginName));
        // 3 生成cos客户端
        return new COSClient(cred, clientConfig);
    }

    @Service
    public class TencentServiceImpl extends AbstractFileStrategy {

        @Override
        public void download(String key, String downName) {
            init();
            String downPath = "/" + downName;
            File downFile = new File(downPath); //自定义下载文件路径或直接填key
            COSClient cosclient = getCOSClient();
            GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);
            ObjectMetadata downObjectMeta = cosclient.getObject(getObjectRequest, downFile);
        }

        @Override
        protected void uploadFile(SystemFile file, MultipartFile multipartFile, String bizType) throws Exception {
            if(multipartFile != null){
                String oldFileName = multipartFile.getOriginalFilename();
                String eName = oldFileName.substring(oldFileName.lastIndexOf("."));
                String newFileName = UUID.randomUUID()+eName;
                COSClient cosclient = getCOSClient();
                File localFile = new File(newFileName);
                try {
                    FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), localFile);
                    // 指定要上传到 COS 上的路径
                    String relativePath = Paths.get(bizType).toString().replaceAll("\\\\", StrPool.SLASH);
                    newFileName = relativePath+StrPool.SLASH+newFileName;
                    String key = StrPool.SLASH+newFileName;
                    PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
                    PutObjectResult putObjectResult = cosclient.putObject(putObjectRequest);

                    String etag = putObjectResult.getETag();
                    FileUtils.forceDelete(localFile);

                    String urlPath = path +newFileName;
                    file.setUrl(StrUtil.replace(urlPath, "\\\\", StrPool.SLASH));
                    file.setFilename(eName);
                    file.setRelativePath(urlPath);
                } catch (IOException e) {
                    log.error("文件上传报错"+e.getMessage());
                }finally {
                    // 关闭客户端(关闭后台线程)
                    cosclient.shutdown();
                }
            }
        }

        @Override
        protected void delete(List<SystemFile> list, SystemFile file) {
            if (file.getRelativePath() != null) {
                COSClient cosclient = getCOSClient();
                cosclient.deleteObject(bucketName, file.getRelativePath());
            }
        }
    }


    @Service
    public class TencentChunkServiceImpl extends AbstractFileChunkStrategy {

        @Override
        public String initMultipartUpload(String fileName, String bizType) {
            COSClient cosclient = getCOSClient();
            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;
            InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, key);
            // 设置存储类型, 默认是标准(Standard), 低频(Standard_IA), 归档(Archive)
            request.setStorageClass(StorageClass.Standard);
            try {
                InitiateMultipartUploadResult initResult = cosclient.initiateMultipartUpload(request);
                // 获取uploadid
                return initResult.getUploadId();
            } catch (CosClientException e) {
                log.error("文件上传报错:", e);
            } finally {
                cosclient.shutdown();
            }
            return null;
        }

        @Override
        public String listPart(String uploadId, String fileName, String bizType) {
            COSClient cosclient = getCOSClient();
            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;

            JSONArray jsonArray = new JSONArray();
            PartListing partListing;
            ListPartsRequest listPartsRequest = new ListPartsRequest(bucketName, key, uploadId);
            do {
                try {
                    partListing = cosclient.listParts(listPartsRequest);
                } catch (CosClientException e) {
                    log.error("获取已上传分片信息失败:", e);
                    throw e;
                }
                for (PartSummary part : partListing.getParts()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.set("partNumber", part.getPartNumber());
                    jsonObject.set("etag", part.getETag());
                    jsonArray.add(jsonObject);
                }
                listPartsRequest.setPartNumberMarker(partListing.getNextPartNumberMarker());
            } while (partListing.isTruncated());
            cosclient.shutdown();
            return jsonArray.toString();
        }

        @Override
        public boolean uploadPart(String uploadId, int partNumber, MultipartFile multipartFile, String bizType,String fileName) {
            COSClient cosclient = getCOSClient();

            String newFileName = fileName;

            String eName = newFileName.substring(newFileName.lastIndexOf("."));
            File localFile = new File(newFileName);

            try {
                FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), localFile);
                newFileName = spliceName(newFileName, bizType);
                String key = StrPool.SLASH + newFileName;

                UploadPartRequest uploadPartRequest = new UploadPartRequest();
                uploadPartRequest.setBucketName(bucketName);
                uploadPartRequest.setKey(key);
                uploadPartRequest.setUploadId(uploadId);
                // 设置分块的数据来源输入流
                uploadPartRequest.setInputStream(Files.newInputStream(localFile.toPath()));
                // 设置分块的长度
                uploadPartRequest.setPartSize(localFile.length()); // 设置数据长度
                uploadPartRequest.setPartNumber(partNumber);     // 分片编号

                UploadPartResult uploadPartResult = cosclient.uploadPart(uploadPartRequest);
                PartETag partETag = uploadPartResult.getPartETag();
                FileUtils.forceDelete(localFile);

                String urlPath = path + newFileName;
                return true;
            } catch (Exception e) {
                log.error("文件上传未知异常：", e);
                return false;
            } finally {
                cosclient.shutdown();
            }
        }

        @Override
        public boolean completePart(FileChunkCompleteDTO record, SystemFile systemFile) {
            String uploadId = record.getUploadId();
            String partETags = record.getPartETags();
            String fileName = record.getFileName();
            String bizType = record.getBizType();

            COSClient cosclient = getCOSClient();
            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;

            JSONArray jsonArray = new JSONArray(partETags);
            List<PartETag> parts = new ArrayList<>();
            for (Object o : jsonArray) {
                JSONObject o1 = (JSONObject) o;
                parts.add(new PartETag(o1.getInt("partNumber"), o1.getStr("etag")));
            }

            // 分片上传结束后，调用complete完成分片上传
            CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest(bucketName, key, uploadId, parts);
            try {
                CompleteMultipartUploadResult completeResult = cosclient.completeMultipartUpload(completeMultipartUploadRequest);
                String etag = completeResult.getETag();
            } catch (CosClientException e) {
                log.error("文件上传未知异常：", e);
                return false;
            } finally {
                cosclient.shutdown();
            }
            return true;
        }

        @Override
        public void abortPartUpload(String uploadId, String fileName, String bizType) {
            COSClient cosclient = getCOSClient();
            fileName = spliceName(fileName, bizType);
            String key = StrPool.SLASH + fileName;

            AbortMultipartUploadRequest abortMultipartUploadRequest = new AbortMultipartUploadRequest(bucketName, key, uploadId);
            try {
                cosclient.abortMultipartUpload(abortMultipartUploadRequest);
            } catch (CosClientException e) {
                e.printStackTrace();
            } finally {
                cosclient.shutdown();
            }
        }

        private String spliceName(String fileName, String bizType){
            String relativePath = Paths.get( bizType).toString().replaceAll("\\\\", StrPool.SLASH);
            fileName = relativePath + StrPool.SLASH + fileName;
            return fileName;
        }
    }
}
