/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.211.131
 Source Server Type    : MySQL
 Source Server Version : 80100
 Source Host           : 192.168.211.131:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80100
 File Encoding         : 65001

 Date: 05/07/2024 11:42:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for system_file
-- ----------------------------
DROP TABLE IF EXISTS `system_file`;
CREATE TABLE `system_file`  (
  `id` bigint NOT NULL COMMENT '主键',
  `submitted_file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '原始文件名',
  `url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '文件访问链接',
  `size` bigint NULL DEFAULT 0 COMMENT '文件大小\n单位字节',
  `relative_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '相对路径 ',
  `file_md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'md5值',
  `context_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '文件类型\n取上传文件的值',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT '唯一文件名',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `biz_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '业务类型',
  `upload_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '大文件分片上传ID',
  `part_total` int NULL DEFAULT NULL COMMENT '分片总数',
  `status` int NULL DEFAULT NULL COMMENT '大文件上传状态：0-未完成，1-已完成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for system_file_part
-- ----------------------------
DROP TABLE IF EXISTS `system_file_part`;
CREATE TABLE `system_file_part`  (
  `id` bigint NOT NULL COMMENT '主键',
  `file_id` bigint NOT NULL COMMENT '原文件ID',
  `size` bigint NULL DEFAULT 0 COMMENT '文件大小',
  `part_num` int NULL DEFAULT NULL COMMENT '分片序号',
  `etag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '分片成功标志',
  `file_md5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '' COMMENT 'md5值',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '大文件分片上传表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
