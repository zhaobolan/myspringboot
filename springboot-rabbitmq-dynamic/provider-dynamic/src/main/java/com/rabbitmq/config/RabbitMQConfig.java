package com.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Properties;

@Slf4j
@Configuration
public class RabbitMQConfig {

    @Resource
    private CachingConnectionFactory connectionFactory;
    @Resource
    private RabbitTemplate rabbitTemplate;

    private static final String BASE_QUEUE_NAME = "weiyiji";
    private static final String DYNAMIC_EXCHANGE = "dynamic_exchange";

    @Bean
    public DirectExchange getDirectExchange() {
        return new DirectExchange(DYNAMIC_EXCHANGE);
    }

    @Bean
    public RabbitAdmin rabbitAdmin() {
        return new RabbitAdmin(connectionFactory);
    }

    /**
     * 添加队列到指定交换机
     */
    public void addQueue(String queueName, String routingKey) {
        // 创建队列
        Queue queue = new Queue(queueName);
        rabbitAdmin().declareQueue(queue);
        // 绑定队列
        rabbitAdmin().declareBinding(BindingBuilder.bind(queue).to(getDirectExchange()).with(routingKey));
        log.info("增加监听队列：" + queueName + " 成功");
    }

    /**
     * 删除队列
     */
    public void deleteQueue(String queueName,String routingKey) {
        Queue queue = new Queue(queueName);
        rabbitAdmin().removeBinding(BindingBuilder.bind(queue).to(getDirectExchange()).with(routingKey));
        rabbitAdmin().deleteQueue(queueName);
    }

    /**
     * 判断队列是否存在
     */
    public boolean isExistQueue(String queueName) {
        boolean flag = true;
        if (queueName != null && !"".equals(queueName)) {
            Properties queueProperties = rabbitAdmin().getQueueProperties(queueName);
            if (queueProperties == null) {
                flag = false;
            }
        } else {
            throw new RuntimeException("队列名称为空");
        }
        return flag;
    }

    /**
     * 发送消息到队列
     */
    public void sendMsg(String message, String routingKey) {
        // 根据路由key值构建出队列名称
        String queueName = BASE_QUEUE_NAME + "_" + routingKey;
        // 判断队列是否存在，不存在则创建，并且绑定上路由key
        if (!isExistQueue(queueName)) {
            addQueue(queueName, routingKey);
        }
        // 发送消息到MQ队列
        rabbitTemplate.convertAndSend(DYNAMIC_EXCHANGE, routingKey, message);
    }
}
