package com.rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@EnableRabbit
@SpringBootApplication
public class ProviderDynamicApplication {

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext run = SpringApplication.run(ProviderDynamicApplication.class, args);
        Environment env = run.getEnvironment();
        System.out.println(
                "\n----------------------------------------------------------\n\t"
                        + "应用 "
                        + env.getProperty("spring.application.name")
                        + " 启动成功! 访问连接:\n\t"
                        + "Swagger文档: \t\thttp://"
                        + InetAddress.getLocalHost().getHostAddress()
                        + ":"
                        + env.getProperty("server.port"));
    }
}
