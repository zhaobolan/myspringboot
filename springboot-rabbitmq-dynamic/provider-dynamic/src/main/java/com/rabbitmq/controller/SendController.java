package com.rabbitmq.controller;

import com.rabbitmq.config.RabbitMQConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class SendController {

    @Resource
    private RabbitMQConfig rabbitMQConfig;
    @GetMapping("/send")
    public String sendMsg(String msg, String routingKey) {
        rabbitMQConfig.sendMsg(msg, routingKey);
        return "发送成功";
    }
}
