package com.rabbitmq.config;

import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageListenerConfig {

    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer(CachingConnectionFactory connectionFactory,
                                                                         MyAckReceiver myAckReceiver) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setConcurrentConsumers(1);
        container.setMaxConcurrentConsumers(5);
        // 设置每次从队列中获取的消息数量
        container.setPrefetchCount(1);
        // 设置手动签收
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.setMessageListener(myAckReceiver);
        // TODO WYJ 查询启动时需要加载的监听队列
        container.setQueueNames("weiyiji_1","weiyiji_2","weiyiji_3","weiyiji_4");
        return container;
    }

    @Bean
    public RabbitAdmin rabbitAdmin(CachingConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }
}