package com.rabbitmq.controller;

import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ListenerController {

    @Resource
    private SimpleMessageListenerContainer simpleMessageListenerContainer;

    @GetMapping("/addListener")
    public String addListener(String queueName) {
        simpleMessageListenerContainer.addQueueNames(queueName);
        return "ok";
    }

    @GetMapping("/removeListener")
    public String removeListener(String queueName) {
        simpleMessageListenerContainer.removeQueueNames(queueName);
        return "ok";
    }
}
