package com.security.config;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * * 授权认证
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/level1/**").hasRole("VIP1")
                .antMatchers("/level2/**").hasRole("VIP2")
                .antMatchers("/level3/**").hasRole("VIP3");
        //http.formLogin() ,默认会跳转到登录页，登录页secity框架自带
        http.formLogin().usernameParameter("user")
                .passwordParameter("pwd")
                .loginPage("/userlogin"); //跳转自己指定登录页
        //http.logout() 退出，默认退出到登录页
        http.logout().logoutSuccessUrl("/");    //退出到指定页面
        http.rememberMe().rememberMeParameter("remeber"); // 记住我功能,登录成功后保存cookie,注销时会删除
    }
    /**
     * *登录认证
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 内存用户验证(5.X版本需添加passwordEncoder)
        auth.inMemoryAuthentication().passwordEncoder(new MyPasswordEncoder())
                .withUser("zhangsan").password("123456").roles("VIP1")
                .and()
                .withUser("lisi").password("123456").roles("VIP2")
                .and()
                .withUser("wangwu").password("123456").roles("VIP3");
    }
}
