package com.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.redis.bean.User;
import com.redis.service.UserService;
import net.minidev.json.JSONUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@SpringBootTest

class SpringbootRedisApplicationTests {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    
    @Test
    void contextLoads() {
        User userById = userService.getUserById(2);
        stringRedisTemplate.opsForValue().set("user-02", JSON.toJSONString(userById));
        System.out.println("OJBK");
    }

    @Test
    void test01() {
        String o = stringRedisTemplate.opsForValue().get("user-02");
//        JSONObject parse = (JSONObject) JSON.parse(o);
//        User user = JSONObject.toJavaObject(parse, User.class);

        User user1 = JSONObject.parseObject(o, User.class);
        System.out.println(user1);
    }

    @Test
    void test02() {
        for (int i = 0; i < 3; i++) {
            String list1 = stringRedisTemplate.opsForList().leftPop("list");
            System.out.println(list1);
        }
    }

    @Test
    void test03() {
        System.out.println(LocalDateTime.now());
        HashOperations<Object, Object, Object> hash = redisTemplate.opsForHash();
        hash.put("user", "user-02", JSON.toJSONString(userService.getUserById(2)));
        // 设置过期时间为10分钟，TimeUnit.SECONDS表示单位为秒
        redisTemplate.expire("user", 62, TimeUnit.SECONDS);
    }

    @Test
    void test04() {
        System.out.println(LocalDateTime.now());
        System.out.println(redisTemplate.opsForHash().get("user", "user-02"));
    }

}
