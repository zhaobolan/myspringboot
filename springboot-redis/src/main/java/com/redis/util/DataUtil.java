package com.redis.util;

import com.redis.bean.Role;
import com.redis.bean.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: wyj
 * @time: 2020/6/17 13:14
 */

public class DataUtil {

    public static List<User> userList = new ArrayList<>();

    public static List<Role> roleList = new ArrayList<>();
    static {
        for (int i = 1; i <= 10; i++) {
            userList.add(new User(i, "赵波蓝" + i, "userCode" + i, "石头村" + i));
        }

        for (int i = 1; i <= 10; i++) {
            roleList.add(new Role(i, "魏义几" + i, "roleCode" + i, "中封存" + i));
        }
    }

}
