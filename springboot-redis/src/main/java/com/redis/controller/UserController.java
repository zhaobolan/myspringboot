package com.redis.controller;

import com.redis.bean.User;
import com.redis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/user/{id}")
    public User getUser(@PathVariable("id") Integer id) {
        return userService.getUserById(id);
    }

    @GetMapping(value = "/updateUser/{id}")
    public User updateUser(@PathVariable("id") Integer id) {

        return userService.updateUserById(id);
    }

    @GetMapping(value = "/deleteUser/{id}")
    public void deleteUser(@PathVariable("id") Integer id) {

        userService.deleteUser(id);
    }

}
