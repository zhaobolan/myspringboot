package com.redis.bean;

import java.io.Serializable;

/**
 * @description:
 * @author: wyj
 * @time: 2020/6/17 13:04
 */

public class User implements Serializable {


    private static final long serialVersionUID = -1064495138521264563L;
    private Integer userId;

    private String userName;

    private String code;

    private String address;


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public User() {
    }

    public User(Integer userId, String userName, String code, String address) {
        this.userId = userId;
        this.userName = userName;
        this.code = code;
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("userId=").append(userId);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
