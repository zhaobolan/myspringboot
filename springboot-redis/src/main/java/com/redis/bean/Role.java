package com.redis.bean;

/**
 * @description:
 * @author: wyj
 * @time: 2020/6/17 13:04
 */

public class Role {

    private Integer roleId;

    private String roleName;

    private String roleCode;

    private String roleAddress;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Role(Integer roleId, String roleName, String roleCode, String roleAddress) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleCode = roleCode;
        this.roleAddress = roleAddress;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleAddress() {
        return roleAddress;
    }

    public void setRoleAddress(String roleAddress) {
        this.roleAddress = roleAddress;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Role{");
        sb.append("roleId=").append(roleId);
        sb.append(", roleName='").append(roleName).append('\'');
        sb.append(", roleCode='").append(roleCode).append('\'');
        sb.append(", roleAddress='").append(roleAddress).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
