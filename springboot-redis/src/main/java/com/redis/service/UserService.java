package com.redis.service;

import com.redis.bean.User;
import com.redis.util.DataUtil;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description:
 * @author: wyj
 * @time: 2020/6/17 13:12
 */
@Service
public class UserService {

//    @Cacheable(cacheNames="user",key = "#id")
    public User getUserById(Integer id) {
        List<User> userList = DataUtil.userList;
        User user = userList.stream().filter(item -> item.getUserId().equals(id)).findFirst().get();
        System.out.println("user value:" + user);
        return user;
    }


//    @CachePut(cacheNames="user",key = "#id")
    public User updateUserById(Integer id) {
        List<User> userList = DataUtil.userList;
        User user = null;
        for (User user1 : userList) {
            if (user1.getUserId().equals(id)) {
                user1.setUserName(user1.getUserName()+"测试问卷啊啊啊");
                user = user1;
                break;
            }
        }
        System.out.println("user value:" + user);
        return user;
    }


//    @CacheEvict(value="user",key = "#id")
    public void deleteUser(Integer id) {

        System.out.println("缓存删除成功");
    }

}
