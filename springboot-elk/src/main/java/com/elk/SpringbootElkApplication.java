package com.elk;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableCaching
public class SpringbootElkApplication {

    private static Logger log = LoggerFactory.getLogger(SpringbootElkApplication.class);
    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(SpringbootElkApplication.class, args);
        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 'elk整合案例' 启动成功! 访问连接:\n\t" +
                        "任务调度中心: \t\thttp://{}:{}\n\t" +
                        "----------------------------------------------------------",
                "127.0.0.1",
                env.getProperty("server.port")
        );
    }

}
