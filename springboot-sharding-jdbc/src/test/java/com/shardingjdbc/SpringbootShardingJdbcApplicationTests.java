package com.shardingjdbc;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.shardingjdbc.config.PreciseTableShardingAlgorithm;
import com.shardingjdbc.dao.BankFlowMapper;
import com.shardingjdbc.dao.UserMapper;
import com.shardingjdbc.entity.BankFlow;
import com.shardingjdbc.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@SpringBootTest
class SpringbootShardingJdbcApplicationTests {

    @Autowired
    private BankFlowMapper bankFlowMapper;

    @Test
    void saveFlow() throws ParseException {
        BankFlow bankFlow = new BankFlow();
        bankFlow.setId(100L);
        bankFlow.setCreateTime(Instant.now());
        bankFlow.setFlowTime(Instant.now());
        bankFlow.setFlowId("ceshi");
        bankFlow.setMoney(new BigDecimal("888.88"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = sdf.parse("2021-04-01");
        bankFlow.setShardingTime(parse.getTime());
        bankFlowMapper.insert(bankFlow);
    }

    @Test
    void list() throws ParseException {
        List<BankFlow> bankFlows = bankFlowMapper.selectList(null);
        for (BankFlow bankFlow : bankFlows) {
            System.out.println(bankFlow);
        }
    }
}
