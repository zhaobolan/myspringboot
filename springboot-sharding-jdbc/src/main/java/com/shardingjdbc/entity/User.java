package com.shardingjdbc.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@TableName(value = "user")
public class User {
    private long id;
    private String nickname;
    private String mobile;
}
