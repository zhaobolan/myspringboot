package com.shardingjdbc.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shardingjdbc.entity.BankFlow;
import org.springframework.stereotype.Repository;

@Repository
public interface BankFlowMapper extends BaseMapper<BankFlow> {
}