package com.shardingjdbc;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@MapperScan("com.zbl.dao")
public class SpringbootShardingJdbcApplication {

    private static Logger log = LoggerFactory.getLogger(SpringbootShardingJdbcApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(SpringbootShardingJdbcApplication.class, args);
        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 'sharding-jdbc整合案例' 启动成功! 访问连接:\n\t" +
                        "任务调度中心: \t\thttp://{}:{}\n\t" +
                        "----------------------------------------------------------",
                "127.0.0.1",
                env.getProperty("server.port")
        );
    }

}
