CREATE TABLE `bank_flow_2021`.money_flow_202201
(
    id            INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    flow_id       VARCHAR(50)    NOT NULL COMMENT '流水id',
    money         DECIMAL(10, 4) NOT NULL COMMENT '金额',
    create_time   TIMESTAMP COMMENT '创建时间',
    flow_time     TIMESTAMP      NOT NULL COMMENT '流水时间',
    sharding_time BIGINT(20) NOT NULL COMMENT '分片时间'
)

