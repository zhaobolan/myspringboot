package com.p6spy;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * SpringBoot整合P6spay工具包案列
 * 本项目采用Mybatis和druid数据源实现数据库读取
 *
 * @author wyj
 * @date 2022/8/16 19:53
 */
@SpringBootApplication
@MapperScan("com.p6spy.dao")
public class SpringbootP6spyApplication {

    private static Logger log = LoggerFactory.getLogger(SpringbootP6spyApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(SpringbootP6spyApplication.class, args);
        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "应用 'P6spay工具包整合案例' 启动成功! 访问连接:\n\t" +
                        "任务调度中心: \t\thttp://{}:{}\n\t" +
                        "----------------------------------------------------------",
                "127.0.0.1",
                env.getProperty("server.port")
        );
    }
}
