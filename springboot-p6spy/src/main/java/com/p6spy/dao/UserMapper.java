package com.p6spy.dao;

import com.p6spy.entity.User;

import java.util.List;

public interface UserMapper {
    // 对应xml映射文件元素的ID
    User selectByPrimaryKey(long id);

    List<User> selectUser();
}
