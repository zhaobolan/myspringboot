package com.p6spy.format;

/**
 * 设置p6spy的SQL打印颜色
 *
 * @author wyj
 * @date 2022/8/17 11:51
 */
public class StdoutLogger extends com.p6spy.engine.spy.appender.StdoutLogger {

    @Override
    public void logText(String text) {
        // 打印红色 SQL 日志
        System.err.println(text);
    }
}
