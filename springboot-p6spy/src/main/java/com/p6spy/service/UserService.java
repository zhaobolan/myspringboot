package com.p6spy.service;


import com.p6spy.entity.User;

import java.util.List;

public interface UserService {
    User getUserById(long userId);

    List<User> listUser(int page, int pageSize);
}
